from django.contrib import admin

from .models import StripeCoupon

# Register your models here.


class StripeCouponAdmin(admin.ModelAdmin):
    fieldsets = [
        (
            "Coupon Info",
            {
                "fields": [
                    "coupon_name",
                    "coupon_id",
                    "creation_date",
                    "redeem_by",
                    "amount_off",
                    "percent_off",
                    "duration",
                    "duration_in_months",
                    "livemode",
                    "times_redeemed",
                    "max_redemptions",
                ]
            },
        ),
        ("Coupon JSON", {"fields": ["coupon_json_formatted"]}),
    ]
    list_display = (
        "coupon_name",
        "coupon_id",
        "creation_date",
        "redeem_by",
        "amount_off",
        "percent_off",
        "duration",
        "duration_in_months",
        "livemode",
        "times_redeemed",
        "max_redemptions",
    )
    readonly_fields = ("coupon_json", "coupon_json_formatted")


admin.site.register(StripeCoupon, StripeCouponAdmin)
