import json

from django.contrib.postgres.fields import JSONField
from django.db import models
from django.utils.safestring import mark_safe
from django.utils.timezone import now
from pygments import highlight
from pygments.formatters.html import HtmlFormatter
from pygments.lexers.data import JsonLexer

# from django.conf import settings


class StripeCoupon(models.Model):
    coupon_name = models.CharField(max_length=200)
    coupon_id = models.CharField(max_length=200)
    creation_date = models.DateTimeField(default=now)
    redeem_by = models.DateTimeField(default=None, null=True)
    amount_off = models.IntegerField(default=None, null=True)
    percent_off = models.IntegerField(default=None, null=True)
    duration = models.CharField(max_length=100)
    duration_in_months = models.IntegerField(default=None, null=True)
    livemode = models.BooleanField(default=None, null=True)
    times_redeemed = models.IntegerField(default=0)
    max_redemptions = models.IntegerField(default=None, null=True)
    coupon_json = JSONField(default=dict)

    def coupon_json_formatted(self):
        # with JSON field, no need to do .loads
        data = json.dumps(self.coupon_json, indent=4)

        # format it with pygments and highlight it
        formatter = HtmlFormatter(style="colorful")
        response = highlight(data, JsonLexer(), formatter)

        # include the style sheet
        style = "<style>" + formatter.get_style_defs() + "</style>"

        return mark_safe(style + response)

    amount_off.short_description = "Amount off (in cents)"
    coupon_json_formatted.short_description = "Customer Data Formatted"

    class Meta:
        verbose_name_plural = "Stripe Coupons"
        verbose_name = "Stripe Coupon"
