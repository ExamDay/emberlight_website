from django.urls import path

from . import views

app_name = "api"


urlpatterns = [
    path("stripe/", views.stripe_webhook_handler),
    path("github/", views.github_webhook_handler),
]
