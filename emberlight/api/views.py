import datetime
import hmac
import json
import logging
import os
import traceback
from hashlib import sha1
from ipaddress import ip_address
from ipaddress import ip_network

import requests
import stripe
from account.models import StripeCustomerData
from api.models import StripeCoupon
from decouple import config
from django.conf import settings
from django.core.mail import send_mail
from django.http import HttpResponse
from django.http import HttpResponseForbidden
from django.http import HttpResponseServerError
from django.utils.encoding import force_bytes
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST

from .known_stripe_events import events as known_events

endpoint_secret = config("STRIPE_WEBHOOK_KEY")
stripe.api_key = settings.STRIPE_SECRET_KEY

env = config("WEBSITE_ENV")
if env == "development":
    logging.basicConfig(
        filename="api_views.log",
        level=logging.DEBUG,
        format="[%(asctime)s|%(name)s|api/views.py|%(levelname)s] %(message)s",
    )
elif env == "production":
    logging.basicConfig(
        filename="/webapps/emberlight/logs/gunicorn_supervisor.log",
        level=logging.INFO,
        format="[%(asctime)s|%(name)s|api/views.py|%(levelname)s] %(message)s",
    )

# Create your views here.


@require_POST
@csrf_exempt
def github_webhook_handler(request):
    # Verify if request came from GitHub
    forwarded_for = "{}".format(request.META.get("HTTP_X_FORWARDED_FOR"))
    client_ip_address = ip_address(forwarded_for)
    whitelist = requests.get("https://api.github.com/meta").json()["hooks"]

    for valid_ip in whitelist:
        if client_ip_address in ip_network(valid_ip):
            break
    else:
        return HttpResponseForbidden("First Permission denied.")

    # Verify the request signature
    header_signature = request.META.get("HTTP_X_HUB_SIGNATURE")
    if header_signature is None:
        return HttpResponseForbidden("Second Permission denied.")

    sha_name, signature = header_signature.split("=")
    if sha_name != "sha1":
        return HttpResponseServerError("Operation not supported.", status=501)

    mac = hmac.new(
        force_bytes(settings.GITHUB_WEBHOOK_KEY), msg=force_bytes(request.body), digestmod=sha1
    )
    if not hmac.compare_digest(force_bytes(mac.hexdigest()), force_bytes(signature)):
        return HttpResponseForbidden("Third Permission denied.")

    # Process the GitHub events
    event = request.META.get("HTTP_X_GITHUB_EVENT", "ping")

    if event == "ping":
        return HttpResponse("pong")
    elif event == "push":
        # Do something...
        # useless comment
        # TODO: Make system call to bash script
        dictionary = json.loads(request.body.decode("utf-8"))
        if (
            dictionary["ref"].split("/")[-1] == "develop"
            or dictionary["ref"].split("/")[-1] == "master"
        ):
            os.system("git pull")
            os.system("../bin/python3 manage.py makemigrations")
            os.system("../bin/python3 manage.py migrate")
            os.system("echo yes | ../bin/python3 manage.py collectstatic")
            os.system("supervisorctl reload")
            return HttpResponse("Success")
            # os.system("chmod +x ../../update.sh")
            # os.system("./../../update.sh")
            # return HttpResponse("Success")

    # In case we receive an event that's neither a ping or push
    return HttpResponse(status=204)


# -- Stripe Event Handlers -- #
def on_charge_failed(event):
    """Occurs whenever a failed charge attempt occurs."""
    print("CHARGE FAILED")
    event_JSON = event.data.object
    print(event_JSON)
    try:
        subscriber = StripeCustomerData.objects.all().get(
            email=event_JSON["billing_details"]["email"]
        )
        subscriber.subscription_json["status"] = "unpaid"
        subscriber.save()
    except Exception:
        name = "on_charge_failed Exception"
        message = "{}\n{}\n\n{}".format(
            settings.EMAIL_HOST_USER, datetime.datetime.now(), traceback.format_exc()
        )
        send_mail(name, message, settings.EMAIL_HOST_USER, ["engineering@torchlightintel.com"])
        traceback.print_exc()
    return HttpResponse(status=200)


def on_charge_succeeded(event):
    """Occurs whenever a new charge is created and is successful."""
    print("CHARGE SUCCEEDED")
    event_JSON = event.data.object
    print(event_JSON)
    # craft and send confirmation email
    try:
        subscriber = StripeCustomerData.objects.all().get(
            email=event_JSON["billing_details"]["email"]
        )
        subscriber.subscription_json["status"] = "active"
        subscriber.save()
        customer_email = event_JSON["billing_details"]["email"]
        subscription_type = subscriber.subscription_json["plan"]["nickname"]
        message = (
            "Thank you for your payment toward your Emberlight "
            + subscription_type
            + " plan.\n\nPurchase Successful!"
        )
        name = "Purchase Confirmation"
        send_mail(name, message, settings.EMAIL_HOST_USER, [customer_email])
    except Exception as e:
        name = "on_charge_succeeded Exception"
        message = "{}\n{}\n\n{}".format(settings.EMAIL_HOST_USER, datetime.datetime.now(), e)
        send_mail(name, message, settings.EMAIL_HOST_USER, ["engineering@torchlightintel.com"])
        print(e)
    return HttpResponse(status=200)


def on_coupon_created(event):
    """Occurs whenever a coupon is created."""
    event_JSON = event.data.object
    creation_date = datetime.datetime.fromtimestamp(event_JSON["created"])
    if event_JSON["redeem_by"] is None:
        redeem_by = creation_date
    elif event_JSON["redeem_by"] is not None:
        redeem_by = datetime.datetime.fromtimestamp(event_JSON["redeem_by"])

    StripeCoupon(
        coupon_name=event_JSON["name"],
        coupon_id=event_JSON["id"],
        creation_date=creation_date,
        redeem_by=redeem_by,
        amount_off=event_JSON["amount_off"],
        percent_off=event_JSON["percent_off"],
        duration=event_JSON["duration"],
        duration_in_months=event_JSON["duration_in_months"],
        livemode=event_JSON["livemode"],
        times_redeemed=event_JSON["times_redeemed"],
        max_redemptions=event_JSON["max_redemptions"],
        coupon_json=json.loads(str(event_JSON)),
    ).save()
    return HttpResponse(status=200)


def on_coupon_deleted(event):
    """Occurs whenever a coupon is deleted."""
    event_JSON = event.data.object
    try:
        StripeCoupon.objects.all().get(coupon_id=event_JSON["id"]).delete()
    except Exception:
        pass
    return HttpResponse(status=200)


def on_coupon_updated(event):
    """Occurs whenever a coupon is updated."""
    event_JSON = event.data.object
    creation_date = datetime.datetime.fromtimestamp(event_JSON["created"])
    if event_JSON["redeem_by"] is None:
        redeem_by = creation_date
    elif event_JSON["redeem_by"] is not None:
        redeem_by = datetime.datetime.fromtimestamp(event_JSON["redeem_by"])

    try:
        coupon_to_change = StripeCoupon.objects.all().get(coupon_id=event_JSON["id"])
    except Exception:
        StripeCoupon(
            coupon_name=event_JSON["name"],
            coupon_id=event_JSON["id"],
            creation_date=creation_date,
            redeem_by=redeem_by,
            amount_off=event_JSON["amount_off"],
            percent_off=event_JSON["percent_off"],
            duration=event_JSON["duration"],
            duration_in_months=event_JSON["duration_in_months"],
            livemode=event_JSON["livemode"],
            times_redeemed=event_JSON["times_redeemed"],
            max_redemptions=event_JSON["max_redemptions"],
            coupon_json=json.loads(str(event_JSON)),
        ).save()
        return HttpResponse(status=200)
    coupon_to_change.coupon_name = event_JSON["name"]
    coupon_to_change.coupon_id = event_JSON["id"]
    coupon_to_change.creation_date = creation_date
    coupon_to_change.redeem_by = redeem_by
    coupon_to_change.amount_off = event_JSON["amount_off"]
    coupon_to_change.percent_off = event_JSON["percent_off"]
    coupon_to_change.duration = event_JSON["duration"]
    coupon_to_change.duration_in_months = event_JSON["duration_in_months"]
    coupon_to_change.livemode = event_JSON["livemode"]
    coupon_to_change.times_redeemed = event_JSON["times_redeemed"]
    coupon_to_change.max_redemptions = event_JSON["max_redemptions"]
    coupon_to_change.coupon_json = json.loads(str(event_JSON))
    coupon_to_change.save()
    return HttpResponse(status=200)


def on_customer_subscription_created(event):
    # craft and send confirmation email
    event_JSON = event.data.object
    try:
        subscription = StripeCustomerData.objects.all().get(id=event_JSON["customer"])
        customer_email = subscription.customer_json["email"]
        subscription_type = event_JSON["items"]["data"][0]["plan"]["nickname"]
        message = (
            "Thank you for subscribing to our Emberlight "
            + subscription_type
            + " plan.\n\nYour free trial period has begun!"
        )
        name = "Purchase Confirmation"
        send_mail(name, message, settings.EMAIL_HOST_USER, [customer_email])
    except Exception:
        name = "on_customer_subscription_created Exception"
        message = "{}\n{}\n\n{}".format(
            settings.EMAIL_HOST_USER, datetime.datetime.now(), traceback.format_exc()
        )
        logging.error("\n" + name + "\n" + message + "\n")
        send_mail(name, message, settings.EMAIL_HOST_USER, ["engineering@torchlightintel.com"])
        traceback.print_exc()
    return HttpResponse(status=200)


def on_customer_subscription_deleted(event):
    """Occurs whenever a customer's subscription ends.
    contains a stripe.PaymentMethod"""
    event_JSON = event.data.object
    subscriptions = StripeCustomerData.objects.all()
    for subscription in subscriptions:
        if subscription.customer_json["id"] == event_JSON["customer"]:
            subscription.subscription_json["status"] = event_JSON["status"]
            subscription.save()
    return HttpResponse(status=200)


# -- End Stripe Event Handlers -- #


@require_POST
@csrf_exempt
def stripe_webhook_handler(request):
    # Verify if request came from Stripe
    if not settings.DEBUG:
        forwarded_for = "{}".format(request.META.get("HTTP_X_FORWARDED_FOR"))
        client_ip_address = ip_address(forwarded_for)
        whitelist = requests.get("https://stripe.com/files/ips/ips_webhooks.json").json()[
            "WEBHOOKS"
        ]
    event = None
    payload = request.body

    # Validate IP Whitelist
    if not settings.DEBUG:
        for valid_ip in whitelist:
            if client_ip_address in ip_network(valid_ip):
                break
        else:
            return HttpResponseForbidden("ERROR: IP did not originate from payment processor.")

    # Verify the request signature
    sig_header = request.META["HTTP_STRIPE_SIGNATURE"]
    if sig_header is None:
        return HttpResponseForbidden("Signature header is none.")

    try:
        event = stripe.Webhook.construct_event(payload, sig_header, endpoint_secret)
    except ValueError:
        # Invalid Payload
        logging.info(traceback.format_exc())
        return HttpResponse(status=400)
    except stripe.error.SignatureVerificationError:
        # Invalid signature
        logging.info(traceback.format_exc())
        return HttpResponse(status=400)
    # Handle the event
    event_handlers = {
        "charge.failed": on_charge_failed,
        "charge.succeeded": on_charge_succeeded,
        "coupon.created": on_coupon_created,
        "coupon.deleted": on_coupon_deleted,
        "coupon.updated": on_coupon_updated,
        "customer.subscription.created": on_customer_subscription_created,
        "customer.subscription.deleted": on_customer_subscription_deleted,
    }
    logging.info("STRIPE EVENT: " + str(event.type))
    # try to handle the event
    try:
        handler = event_handlers[event.type]
        return handler(event)
    except KeyError:
        if event.type in known_events:
            return HttpResponse(status=200)
        else:
            logging.info("Unrecognized Event")
            # push back if unknown event type recieved
            name = "Unknown Stripe Event"
            message = "{}\n{}\n\n{}".format(
                settings.EMAIL_HOST_USER, datetime.datetime.now(), event.type
            )
            logging.error("\n" + name + "\n" + message + "\n")
            send_mail(name, message, settings.EMAIL_HOST_USER, ["engineering@torchlightintel.com"])
            return HttpResponse(status=400)
    # In case we receive an event that's neither a ping or push
    logging.info("Event Handled")
    return HttpResponse(status=200)
