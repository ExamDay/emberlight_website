import ast
import datetime
import json
import time
import traceback

import dateutil.relativedelta
import redis
import stripe
from account.models import MiscUserInfo
from account.models import StripeCustomerData
from api.models import StripeCoupon
from decouple import config
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.mail import BadHeaderError
from django.core.mail import send_mail
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.shortcuts import redirect
from django.shortcuts import render
from django.views.decorators.http import require_GET
from django.views.decorators.http import require_POST

from .forms import ContactForm
from .forms import PricingForm

# -- Database Settings -- #
rdb = redis.Redis(
    host="localhost", port=6379, db=0, password=config("REDIS_PASSWORD"), decode_responses=True
)


# Do most of the logic here before rendering
stripe.api_key = settings.STRIPE_SECRET_KEY


def home_page(request):
    if request.method == "POST":
        form = ContactForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data.get("name")
            from_email = form.cleaned_data.get("email")
            message = form.cleaned_data.get("message")
            message = from_email + "\n\n" + message
            try:
                send_mail(name, message, settings.EMAIL_HOST_USER, ["support@torchlightintel.com"])
            except BadHeaderError:
                return HttpResponse("Invalid header found.")
            return HttpResponseRedirect("/contact/thanks/")
        else:
            return HttpResponse("Make sure all fields are filled out and valid.")
    title = "Emberlight - Home"
    context = {"title": title}
    return render(request, "main/home.html", context)


def contact_page(request):
    if request.method == "POST":
        form = ContactForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data.get("name")
            from_email = form.cleaned_data.get("email")
            message = form.cleaned_data.get("message")
            message = from_email + "\n\n" + message
            print(name, from_email, message)
            try:
                send_mail(name, message, settings.EMAIL_HOST_USER, ["support@torchlightintel.com"])
            except BadHeaderError:
                return HttpResponse("Invalid header found.")
            return HttpResponseRedirect("/contact/thanks/")
        else:
            return HttpResponse("Make sure all fields are filled out and valid.")
    title = "Emberlight - Contact Us"
    return render(request, "main/contact.html", {"title": title})


@require_GET
def robots_txt(request):
    if settings.DEBUG:
        robots_txt_contents = ["User-Agent: *", "Disallow: /"]
    elif not settings.DEBUG:
        robots_txt_contents = ["User-Agent: *", "Disallow:"]
    return HttpResponse("\n".join(robots_txt_contents), content_type="text/plain")


def contact_thanks(request):
    title = "Emberlight - Thank You"
    return render(request, "main/contact-thanks.html", {"title": title})


def support_page(request):
    title = "Emberlight - Support"
    return render(request, "main/support.html", {"title": title})


def about_page(request):
    title = "Emberlight - About Us"
    return render(request, "main/about.html", {"title": title})


def payment_success(request):
    title = "Emberlight - Purchase Success!"
    return render(request, "main/payment-success.html", {"title": title})


def terms_of_service(request):
    return render(request, "main/terms-of-service.html")


def privacy_policy(request):
    return render(request, "main/privacy-policy.html")


def presskit(request):
    return render(request, "main/presskit.html")


def pricing_page(request):
    try:
        referral_code = request.GET["ref"]
    except Exception:
        referral_code = ""
    title = "Emberlight - Pricing"
    context = {"title": title, "referral_code": referral_code}
    if not request.user.is_authenticated:
        if referral_code == "":
            return redirect("/account/register/", context)
        else:
            return redirect("/account/register/?ref={}".format(referral_code), context)
    return render(request, "main/pricing.html", context)


def referral_code(request, coupon_id):
    referral_code = get_object_or_404(StripeCoupon, coupon_id=coupon_id)
    return redirect("/pricing/?ref={}".format(referral_code.coupon_id))


@login_required
def checkout(request):
    try:
        sub_name = request.GET["sub_name"]
    except Exception:
        return redirect("main/pricing.html")
        print("\n\nno sub_name query in URL\n\n")
    try:
        referral_code = request.GET["ref"]
    except Exception:
        referral_code = ""
    try:
        title = "Emberlight - Checkout"
        form = PricingForm(initial={"couponcode": referral_code})
        plan = settings.PLANS[
            sub_name
        ]  # variable "plan" is a dictionary with info about subscriptions.
        context = {
            "title": title,
            "STRIPE_PUBLISHABLE_KEY": settings.STRIPE_PUBLISHABLE_KEY,
            "form": form,
            "referral_code": referral_code,
            "sub_id": plan["stripe_id"],
            "price": plan["price"],
            "total_price": plan["total_price"],
            "payment_period": plan["payment_period"],
            "plan_name": plan["nickname"],
        }
        return render(request, "main/checkout.html", context)
    except KeyError:
        name = "THREAT: Invalid Payment Plan"
        message = "{}\n{}\n\n{}\n\n{}\n{}\n\nTraceback: {}".format(
            settings.EMAIL_HOST_USER,
            datetime.datetime.now(),
            "Someone tried to request a payment plan we don't support.",
            request.user,
            request.user.email,
            traceback.format_exc(),
        )
        send_mail(name, message, settings.EMAIL_HOST_USER, ["engineering@torchlightintel.com"])
        messages.error(request, "Invalid payment plan. This incident has been reported.")
        title = "Emberlight - Pricing"
        context = {"title": title, "key": settings.STRIPE_PUBLISHABLE_KEY}
        return redirect("/pricing", context)


@login_required
@require_POST
def payment(request):
    form = PricingForm(request.POST)
    # -- Data Validation -- #
    # Check to make sure the user is logged in
    if not request.user.is_authenticated:
        messages.error(request, "You must be logged in to subscribe.")
        title = "Emberlight - Pricing"
        context = {"title": title, "key": settings.STRIPE_PUBLISHABLE_KEY}
        return redirect(
            "/account/register/?next=/pricing/&ref={}".format(
                form.cleaned_data.get("couponcode") if form.is_valid() else ""
            ),
            context,
        )

    # Check to see if the user is already subscribed to a payment plan
    try:
        user_ID = StripeCustomerData.objects.all().get(username=request.user).customer_json
    except StripeCustomerData.DoesNotExist:
        user_ID = None
    try:
        subscription_ID = (
            StripeCustomerData.objects.all().get(username=request.user).subscription_json
        )
        if subscription_ID["status"] == "unpaid":
            messages.error(
                request,
                "There was an error the last time we tried to charge your card. Check your inbox for any emails from support@torchlightintel.com.",
            )
            title = "Emberlight - Home"
            context = {"title": title, "key": settings.STRIPE_PUBLISHABLE_KEY}
            return redirect("/", context)

    except StripeCustomerData.DoesNotExist:
        subscription_ID = None

    # user is created and subscribed
    # user is created and not subscribed
    # user is not created and subscribed (Can't happen!)
    # user is not created and not subscribed
    # Check for if the user has a subscription. If both checks fail, it is assumed that either the
    #   user exists but doesn't have an active subscription or the user doesn't exist and doesn't
    #   have a subscription.
    if user_ID and subscription_ID:
        if subscription_ID["status"] == "active":
            messages.error(request, "You already have a subscription.")
            title = "Emberlight - Home"
            context = {"title": title, "key": settings.STRIPE_PUBLISHABLE_KEY}
            return redirect("/", context)
    elif not user_ID and subscription_ID:
        print(
            "The user is not created and yet has a subscription. Something has gone terribly wrong."
        )

    # Handle coupons
    if form.is_valid():
        couponcode = form.cleaned_data.get("couponcode")
        stripe_coupon_code = ""
        trial_period_months = 1
        try:
            logged_in_user = MiscUserInfo.objects.all().get(username=request.user)
        except MiscUserInfo.DoesNotExist:
            if settings.DEBUG:
                MiscUserInfo(username=request.user.username, email=request.user.email).save()
                logged_in_user = MiscUserInfo.objects.all().get(username=request.user)
            elif not settings.DEBUG:
                messages.error(request, "There was an error processing your payment.")
                title = "Emberlight - Pricing"
                context = {"title": title, "key": settings.STRIPE_PUBLISHABLE_KEY}
                return redirect("/pricing", context)

        # Check for unique IndieGoGo and media specific codes
        with open("unique_coupon_codes.txt", "r+") as f:
            unique_coupon_codes = ast.literal_eval(f.read())
        if couponcode not in unique_coupon_codes:
            # messages.error(request, "Coupon code invalid.")
            try:
                stripe_coupon_code = StripeCoupon.objects.all().get(coupon_id=couponcode).coupon_id
                # -- Check to see if the coupon code has a maximum amount of redemptions -- #
                if StripeCoupon.objects.all().get(coupon_id=couponcode).max_redemptions is not None:
                    if (
                        StripeCoupon.objects.all().get(coupon_id=couponcode).times_redeemed
                        >= StripeCoupon.objects.all().get(coupon_id=couponcode).max_redemptions
                    ):
                        messages.error(
                            request, "That coupon code has reached its redemption limit :c"
                        )
                        title = "Emberlight - Pricing"
                        context = {"title": title, "key": settings.STRIPE_PUBLISHABLE_KEY}
                        return redirect("/pricing", context)
                    else:
                        increment = StripeCoupon.objects.all().get(coupon_id=couponcode)
                        increment.times_redeemed += 1
                        increment.save()

            except StripeCoupon.DoesNotExist:
                if couponcode == "":
                    pass
                elif couponcode != "":
                    messages.error(request, "Coupon code invalid!")
                    title = "Emberlight - Pricing"
                    context = {"title": title, "key": settings.STRIPE_PUBLISHABLE_KEY}
                    return redirect("/pricing", context)
        elif couponcode in unique_coupon_codes:
            if couponcode.startswith("INDIEGOGO-1"):
                # GREEN TIER
                # Green tier doesn't get a stripe coupon code because it is only a free month
                #   and doesn't include any discounts
                if request.POST.get("plan") != "Monthly":
                    messages.error(
                        request, "This coupon code is not available for this pricing plan!"
                    )
                    title = "Emberlight - Pricing"
                    context = {"title": title, "key": settings.STRIPE_PUBLISHABLE_KEY}
                    return redirect("/pricing", context)
                trial_period_months = 1
            elif couponcode.startswith("INDIEGOGO-2"):
                # GREEN TIER PLUS
                if request.POST.get("plan") != "Monthly":
                    messages.error(
                        request, "This coupon code is not available for this pricing plan!"
                    )
                    title = "Emberlight - Pricing"
                    context = {"title": title, "key": settings.STRIPE_PUBLISHABLE_KEY}
                    return redirect("/pricing", context)
                stripe_coupon_code = "indiegogo_green_tier_plus"
                trial_period_months = 1
            elif couponcode.startswith("INDIEGOGO-3"):
                # BLUE TIER
                if request.POST.get("plan") != "Monthly":
                    messages.error(
                        request, "This coupon code is not available for this pricing plan!"
                    )
                    title = "Emberlight - Pricing"
                    context = {"title": title, "key": settings.STRIPE_PUBLISHABLE_KEY}
                    return redirect("/pricing", context)
                stripe_coupon_code = "indigogo_blue_tier"
                trial_period_months = 2
            elif couponcode.startswith("INDIEGOGO-4"):
                # BLUE TIER PLUS
                if request.POST.get("plan") != "Monthly":
                    messages.error(
                        request, "This coupon code is not available for this pricing plan!"
                    )
                    title = "Emberlight - Pricing"
                    context = {"title": title, "key": settings.STRIPE_PUBLISHABLE_KEY}
                    return redirect("/pricing", context)
                stripe_coupon_code = "indiegogo_blue_tier_plus"
                trial_period_months = 2
            elif couponcode.startswith("INDIEGOGO-5"):
                # RED TIER
                if request.POST.get("plan") != "Yearly":
                    messages.error(
                        request, "This coupon code is not available for this pricing plan!"
                    )
                    title = "Emberlight - Pricing"
                    context = {"title": title, "key": settings.STRIPE_PUBLISHABLE_KEY}
                    return redirect("/pricing", context)
                stripe_coupon_code = "indiegogo_red_tier"
                trial_period_months = 12
            elif couponcode.startswith("INDIEGOGO-6"):
                # RED TIER PLUS
                if request.POST.get("plan") != "Yearly":
                    messages.error(
                        request, "This coupon code is not available for this pricing plan!"
                    )
                    title = "Emberlight - Pricing"
                    context = {"title": title, "key": settings.STRIPE_PUBLISHABLE_KEY}
                    return redirect("/pricing", context)
                stripe_coupon_code = "indiegogo_red_tier_plus"
                trial_period_months = 24
            elif couponcode.startswith("INDIEGOGO-7"):
                # ORANGE TIER
                stripe_coupon_code = "indiegogo_orange_tier_plus"
                trial_period_months = 0

            logged_in_user.indiegogo_backer = True if couponcode.startswith("INDIEGOGO") else False
            logged_in_user.account_coupon = couponcode
            logged_in_user.media_account = True if couponcode.startswith("MEDIA") else False
            logged_in_user.unix_timestamp_temp_access_start = int(time.time())
            logged_in_user.allow_temporary_access = (
                True if couponcode.startswith("MEDIA") else False
            )

        trial_period_in_days = (
            (
                datetime.date.today()
                + dateutil.relativedelta.relativedelta(months=trial_period_months)
            )
            - datetime.date.today()
        ).days

    # -- Parse Valid Info -- #
    requested_plan = request.POST.get("plan")
    try:
        plan_id = settings.PLANS[requested_plan]["stripe_id"]
    except KeyError:
        name = "THREAT: Invalid Payment Plan"
        message = "{}\n{}\n\n{}\n\n{}\n{}\n\nTraceback: {}".format(
            settings.EMAIL_HOST_USER,
            datetime.datetime.now(),
            "Someone tried to request a payment plan we don't support.",
            request.user,
            request.user.email,
            traceback.format_exc(),
        )
        send_mail(name, message, settings.EMAIL_HOST_USER, ["engineering@torchlightintel.com"])
        messages.error(request, "Invalid payment plan. This incident has been reported.")
        title = "Emberlight - Pricing"
        context = {"title": title, "key": settings.STRIPE_PUBLISHABLE_KEY}
        return redirect("/pricing", context)

    # Create a brand new customer ID if it doesn't exist already, otherwise get the already created
    #   customer ID.
    if not user_ID:
        customer_data = stripe.Customer.create(
            description=request.user.email,
            source=request.POST["stripeToken"],
            email=request.user.email,
        )
    elif user_ID:
        customer_data = StripeCustomerData.objects.all().get(username=request.user).customer_json

    # Sign the user up for a subscription
    print("TRIAL PERIOD IN DAYS: {}".format(trial_period_in_days))

    if not subscription_ID or subscription_ID["status"] == "canceled":
        try:
            subscription_data = stripe.Subscription.create(
                customer=customer_data["id"],
                items=[{"plan": plan_id}],
                trial_period_days=trial_period_in_days,
                coupon=stripe_coupon_code,
                payment_behavior="error_if_incomplete",
            )
        except Exception as e:
            print(e)
            messages.error(
                request,
                "There was an error with your payment information. Please make sure your card is valid.",
            )
            title = "Emberlight - Pricing"
            context = {"title": title, "key": settings.STRIPE_PUBLISHABLE_KEY}
            return redirect("/pricing", context)
    else:
        name = "Error in Payment function"
        message = "{}\n{}\n\n{}\n\n{}\n{}".format(
            settings.EMAIL_HOST_USER,
            datetime.datetime.now(),
            "There was a problem when trying to sign user up for a subscription.",
            request.user,
            request.user.email,
        )
        send_mail(name, message, settings.EMAIL_HOST_USER, ["engineering@torchlightintel.com"])
        messages.error(
            request,
            "There was an error with your payment information. Please make sure your card is valid",
        )
        title = "Emberlight - Pricing"
        context = {"title": title, "key": settings.STRIPE_PUBLISHABLE_KEY}
        return redirect("/pricing", context)

    # Put the user in our database
    StripeCustomerData(
        username=request.user,
        email=request.user.email,
        subscription_active=True,
        subscription_type=request.POST.get("plan"),
        customer_json=json.loads(str(customer_data)),
        subscription_json=json.loads(str(subscription_data)),
    ).save()

    if couponcode in unique_coupon_codes:
        unique_coupon_codes.remove(couponcode)
        with open("unique_coupon_codes.txt", "w+") as f:
            f.write(str(unique_coupon_codes))

    context = {"payment_plan": "Thank you for subscribing to the {} plan!".format(plan_id)}
    return redirect("/payment/success", context)


def test(request):
    title = "Emberlight - Test"
    return render(request, "main/test.html", {"title": title})
