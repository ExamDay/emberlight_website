from account.models import MiscUserInfo as AccountMiscUserInfo
from django.contrib import admin

from .models import MiscUserInfo
from .models import StripeCustomerData

# Register your models here.


def migrate_misc_user_info(modeladmin, request, queryset):
    misc_user_object = MiscUserInfo.objects.all()
    for user in misc_user_object:
        # print(dir(user))
        try:
            AccountMiscUserInfo.objects.all().get(username=user.username)
        except AccountMiscUserInfo.DoesNotExist:
            AccountMiscUserInfo.objects.create(
                username=user.username,
                email=user.email,
                email_list=user.email_list,
                pre_alpha=user.pre_alpha,
                alpha=user.alpha,
                beta=user.beta,
                indiegogo_backer=user.indiegogo_backer,
                account_coupon=user.account_coupon,
                media_account=user.media_account,
                unix_timestamp_temp_access_start=user.unix_timestamp_temp_access_start,
                allow_temporary_access=user.allow_temporary_access,
            )
        except Exception as e:
            print(e)

    # queryset.update(status="published")


migrate_misc_user_info.short_description = (
    "Move misc user information from `main` to `account` table."
)


class StripeCustomerDataAdmin(admin.ModelAdmin):
    fieldsets = [
        (
            "User Info",
            {
                "fields": [
                    "username",
                    "email",
                    "subscription_active",
                    "subscription_start",
                    "subscription_type",
                ]
            },
        ),
        ("Customer Data", {"fields": ["customer_json_formatted", "subscription_json_formatted"]}),
    ]
    list_display = ("username", "subscription_active", "subscription_start", "subscription_type")
    readonly_fields = (
        "username",
        "email",
        "subscription_active",
        "subscription_start",
        "subscription_type",
        "customer_json",
        "subscription_json",
        "customer_json_formatted",
        "subscription_json_formatted",
    )


class MiscUserInfoAdmin(admin.ModelAdmin):
    fieldsets = [
        (
            "User's Information",
            {
                "fields": [
                    "username",
                    "email",
                    "email_list",
                    "pre_alpha",
                    "alpha",
                    "beta",
                    "indiegogo_backer",
                    "account_coupon",
                    "media_account",
                    "unix_timestamp_temp_access_start",
                    "allow_temporary_access",
                ]
            },
        )
    ]
    list_display = (
        "username",
        "email",
        "email_list",
        "pre_alpha",
        "alpha",
        "beta",
        "indiegogo_backer",
        "account_coupon",
        "media_account",
        "unix_timestamp_temp_access_start",
        "allow_temporary_access",
    )
    actions = [migrate_misc_user_info]


admin.site.register(StripeCustomerData, StripeCustomerDataAdmin)
admin.site.register(MiscUserInfo, MiscUserInfoAdmin)
