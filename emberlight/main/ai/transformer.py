"""
    code by TaeHwan Jung(@graykode)
    Original Paper and repository here : https://github.com/openai/gpt-2
    GPT2 Pytorch Model : https://github.com/huggingface/pytorch-pretrained-BERT
"""
# import argparse
import os
import queue
import random
import re
import sys
import threading
import time

import numpy as np
import torch

if __name__ != "__main__":
    from .GPT2.config import GPT2Config
    from .GPT2.encoder import get_encoder
    from .GPT2.model import GPT2LMHeadModel
    from .GPT2.sample import sample_sequence
    from .GPT2.utils import load_weight
elif __name__ == "__main__":
    from GPT2.config import GPT2Config
    from GPT2.encoder import get_encoder
    from GPT2.model import GPT2LMHeadModel
    from GPT2.sample import sample_sequence
    from GPT2.utils import load_weight

my_queue = queue.Queue()


def storeInQueue(f, **kwargs):
    def wrapper(*args, **kwargs):
        my_queue.put(f(*args, **kwargs))

    return wrapper


def text_generator(
    text="",
    quiet=False,
    nsamples=1,
    unconditional=None,
    batch_size=-1,
    length=-1,
    temperature=0.7,
    top_k=40,
    num_threads=1,
):
    print("STARTED TEXT GENERATOR")
    if os.path.exists("emberlight/ai/ai_models/gpt2-pytorch_model.bin"):
        state_dict = torch.load(
            "emberlight/ai/ai_models/gpt2-pytorch_model.bin",
            map_location="cpu" if not torch.cuda.is_available() else None,
        )
    else:
        print("Please download gpt2-pytorch_model.bin and/or place in bin folder")
        sys.exit()

    if batch_size == -1:
        batch_size = 1
    assert nsamples % batch_size == 0

    seed = random.randint(0, 2147483647)
    np.random.seed(seed)
    torch.random.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    print("CUDA AVAILABILITY: {}".format(torch.cuda.is_available()))
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    # Load Model
    enc = get_encoder()
    config = GPT2Config()
    model = GPT2LMHeadModel(config)
    model = load_weight(model, state_dict)
    model.share_memory()
    model.to(device)
    model.eval()

    if length == -1:
        length = config.n_ctx // 2
    elif length > config.n_ctx:
        raise ValueError("Can't get samples longer than window size: %s" % config.n_ctx)

    context_tokens = enc.encode(text)

    list_of_threads = []
    print("THREADCOUNT = {}".format(num_threads))

    for i in range(num_threads):
        kwargs = {
            "model": model,
            "length": length,
            "context": context_tokens if not unconditional else None,
            "start_token": enc.encoder["<|endoftext|>"] if unconditional else None,
            "batch_size": batch_size,
            "temperature": temperature,
            "top_k": top_k,
            "device": device,
            "nsamples": nsamples,
            "unconditional": unconditional,
            "context_tokens": context_tokens,
            "enc": enc,
            "quiet": quiet,
            "num_threads": num_threads,
        }
        thr = threading.Thread(target=runit, kwargs=kwargs)
        thr.daemon = True
        thr.name = "Text Gen Thread {}".format(i)
        thr.start()
        list_of_threads.append(thr)
        print("Started Thread {}!".format(thr.name))

    for t in list_of_threads:
        t.join()

    data = my_queue.get()
    return data


@storeInQueue
def runit(
    model=None,
    length=None,
    context=None,
    start_token=None,
    batch_size=None,
    temperature=None,
    top_k=None,
    device=None,
    nsamples=None,
    unconditional=None,
    context_tokens=None,
    enc=None,
    quiet=None,
    num_threads=None,
):
    global WORD_COUNT_LIST
    generated = 0
    batch_size = batch_size
    start_time = time.time()
    for _ in range(nsamples // batch_size):
        out = sample_sequence(
            model=model,
            length=length,
            context=context_tokens if not unconditional else None,
            start_token=enc.encoder["<|endoftext|>"] if unconditional else None,
            batch_size=batch_size,
            temperature=temperature,
            top_k=top_k,
            device=device,
        )
        out = out[:, len(context_tokens) :].tolist()
        for i in range(batch_size):
            generated += 1
            text = enc.decode(out[i])
            if quiet is False:
                print("=" * 40 + " SAMPLE " + str(generated) + " " + "=" * 40)
                pass
            if text.startswith("<|endoftext|>"):
                continue
            else:
                text = text.split("<|endoftext|>")
            # print(type(text))
            # print(text)

            text = text[0]
            # print(type(text))
            # print(text)
            # print("-"*40)

            regex_pattern = re.compile(r"(\s)((.|\n)+)\s\2")
            final = re.sub(regex_pattern, " ", text)
            final = re.sub(regex_pattern, "", final)
            # print("!"*40)
            # print(type(final))
            # print(final)
            return final

            # WORD_COUNT_LIST.append(len(text.split()))

    """
    # words per seconds that it takes a thread to get through
    writing speed of any particular thread
    """

    total_time_taken = time.time() - start_time

    # total_word_count = sum(i for i in WORD_COUNT_LIST)
    # average_word_count = sum(i for i in WORD_COUNT_LIST) / num_threads
    # word_per_second_batchwise = len(text.split()) / total_time_taken

    print("TOTAL TIME TAKEN FOR {} THREADS IN PARALLEL: {}".format(num_threads, total_time_taken))
    # print("TOTAL WORD COUNT: {}".format(total_word_count))
    # print("AVERAGE WORD COUNT: {}".format(average_word_count))
    # print("WORD PER SECOND BATCHWISE: {}".format(word_per_second_batchwise))

    # clears GPU cache
    torch.cuda.empty_cache()


if __name__ == "__main__":
    WORD_COUNT_LIST = []
    nsamples = 10
    text_to_process = """We know just how to protect Salt Lake City residences with a home insurance policy. Earned through years of listening to the concerns of other local homeowners, our agents can answer any questions you may have about various coverage programs and policy options."""

    for i in range(1, 2):
        # print(i)
        text = text_generator(text=text_to_process, nsamples=nsamples, quiet=False, num_threads=i)
        print(text)
    print("######################### FINISHED #########################")
