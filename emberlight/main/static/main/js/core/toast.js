var toast_initiated = new Event('toast_initiated');
var messages = [];
var messaged = new Event('messaged');

const Toast = {
    init() {
        this.hideTimeout = null;
        this.element = document.createElement('div');
        this.element.className = 'toast';
        document.body.appendChild(this.element);
        document.dispatchEvent(toast_initiated);
    },

    async trigger(message, type) {
        await new Promise(r => setTimeout(r, 500));
        clearTimeout(this.hideTimeout);
        this.element.textContent = message;
        this.element.className = 'toast toast-visible'
        if (type) {
            this.element.classList.add('toast-' + type);
        }
        this.hideTimeout = setTimeout(() => {
            this.element.classList.remove('toast-visible')
        }, 3000)
    },
};

window.addEventListener('DOMContentLoaded', () => Toast.init());
