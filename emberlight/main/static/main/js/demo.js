// Getting Demo Info //
var demoOutput = document.getElementById("demo-output-field");
var m_demoOutput = document.getElementById("m_demo-output-field");

var cartridge0 = document.getElementById("cartridge0");
var cartridge1 = document.getElementById("cartridge1");
var cartridge2 = document.getElementById("cartridge2");
var cartridge3 = document.getElementById("cartridge3");
var cartridge4 = document.getElementById("cartridge4");
var cartridge5 = document.getElementById("cartridge5");


var m_cartridge0 = document.getElementById("m_cartridge0");
var m_cartridge1 = document.getElementById("m_cartridge1");
var m_cartridge2 = document.getElementById("m_cartridge2");
var m_cartridge3 = document.getElementById("m_cartridge3");
var m_cartridge4 = document.getElementById("m_cartridge4");
var m_cartridge5 = document.getElementById("m_cartridge5");

var demo_header_blinker = document.getElementById("demo_header_blinker");
var demo_header_rework = document.getElementById("demo_header_rework")
var rework_button = document.getElementById("rework_button")
var m_demo_header_blinker = document.getElementById("m_demo_header_blinker");
var m_demo_header_rework = document.getElementById("m_demo_header_rework")
var m_rework_button = document.getElementById("m_rework_button")

var instruction = "No choice yet.";
var last_instruction = "Nothing yet.";
var demo_active = false;
var current_worker = "none";
var manual_scrolling = false;

// Check Standards //
var supportsVibrate = "vibrate" in navigator;

// Connection Code //
var loc = window.location;
var wsStart = "ws://";
if (loc.protocol == "https:") {
  var wsStart = "wss://"
}

var endpoint = wsStart + loc.host + '/ws/demo/'
var demoSocket = new WebSocket(endpoint);

demoSocket.onmessage = async function(e) {
  // console.log("output recieved", e)
  var data = JSON.parse(e.data);
  var command = data['command'];
  var output = data['instruction'];

  if (command == "write") {
    if (window.demo_active) {
      for (i = 0; i < output.length; i++) {
        if (output[i] == "\n") {
          output[i] = "<br>"
        } else {
          //pass
        }
      }
      document.getElementById('demo-output-field').innerHTML += (output);
    } else {
      //pass
    }
  } else if (command == "clear") {
    document.getElementById('demo-output-field').innerHTML = ("");
  } else {
    console.log("Unknown Command: " + command)
  }
  equalize_output_fields(demoOutput, m_demoOutput);
  autoScroll(m_demoOutput, 15);
};

demoSocket.onclose = async function(e) {
  console.log('demo socket closed unexpectedly', e);
};

demoSocket.onopen = async function(e) {
  // console.log('open', e);
};

demoSocket.onerror = async function(e) {
  console.log('error', e);
};

// Demo Code //
async function equalize_output_fields(source_element, target_element) {
  target_element.innerHTML = source_element.innerHTML;
};

m_demoOutput.addEventListener('touchmove', async function() {
  manual_scrolling = true;
}, { passive: true });

async function autoScroll(element, cushion) {
  if (window.manual_scrolling == true) {
    // console.log("ST: " + element.scrollTop)
    // console.log("SH: " + element.scrollHeight)
    // console.log("H: " + element.clientHeight)
    // difference = (element.scrollHeight - element.scrollTop)-(element.clientHeight);
    if (((element.scrollHeight - element.scrollTop) - element.clientHeight) < cushion) {
      window.manual_scrolling = false;
      element.scrollTop = element.scrollHeight;
    } else {
      // pass
    }
  } else {
    element.scrollTop = element.scrollHeight;
  };
};

// async function scrollIntoView(elementId) {
//   const scrollIntoViewOptions = { behavior: "smooth", block: "center" };
//   document.getElementById(elementId).scrollIntoView(scrollIntoViewOptions);
// };

var cartridges = [cartridge0, cartridge1, cartridge2, cartridge3, cartridge4, cartridge5];
var prompt_request_for = { "cartridge0": "bitcoin", "cartridge1": "facialrecognition", "cartridge2": "nuclearenergy", "cartridge3": "coronavirus", "cartridge4": "space", "cartridge5": "createyourown" };

async function select(cartridge) {
  window.demo_header_blinker.classList.add("hidden")
  window.demo_header_rework.classList.remove("hidden")
  // console.log("understood: " + cartridge.id);
  window.demo_active = false;
  window.manual_scrolling = false;
  await demoSocket.send(JSON.stringify({
    'command': "stop",
    "worker": window.current_worker,
    "instruction": "die"
  }));
  // console.log("Requesting: 'stop worker': " + current_worker);

  for (i = 0; i < cartridges.length; i++) {
    if (cartridge.id !== cartridges[i].id) {
      cartridges[i].classList.remove("transform-active");
    } else {
      //pass
    }
  };

  if (cartridge.classList.contains("transform-active")) {
    cartridge.classList.remove("transform-active");
  } else {
    window.demo_active = true;
    cartridge.classList.add("transform-active");
    if (window.demo_active) {
      instruction = prompt_request_for[cartridge.id];
      window.last_instruction = instruction;
      // console.log("Requesting: " + instruction);
      window.current_worker = new_worker();
      // console.log("From Worker: " + current_worker);
      await demoSocket.send(JSON.stringify({
        "command": "stop",
        "worker": window.current_worker,
        'instruction': "die"
      }));
      await demoSocket.send(JSON.stringify({
        "command": "start",
        "worker": window.current_worker,
        'instruction': instruction
      }));
    } else {
      //pass
    }
  }
}

var m_cartridges = [m_cartridge0, m_cartridge1, m_cartridge2, m_cartridge3, m_cartridge4, m_cartridge5];
var m_prompt_request_for = { "m_cartridge0": "bitcoin", "m_cartridge1": "facialrecognition", "m_cartridge2": "nuclearenergy", "m_cartridge3": "coronavirus", "m_cartridge4": "space", "m_cartridge5": "createyourown" };

async function m_select(m_cartridge) {
  if (window.supportsVibrate == true) {
    window.navigator.vibrate([20, 20, 50]);
  };
  window.m_demo_header_blinker.classList.add("hidden")
  window.m_demo_header_rework.classList.remove("hidden")
  window.demo_active = false;
  window.manual_scrolling = false;
  await demoSocket.send(JSON.stringify({
    'command': "stop",
    "worker": window.current_worker,
    "instruction": "die"
  }));
  // console.log("Requesting: 'stop worker': " + current_worker);

  for (i = 0; i < m_cartridges.length; i++) {
    if (m_cartridge.id !== m_cartridges[i].id) {
      m_cartridges[i].classList.remove("m_top-transform-active");
      m_cartridges[i].classList.remove("m_bottom-transform-active");
    } else {
      //pass
    }
  };

  if ((m_cartridge.classList.contains("m_top-transform-active")) || (m_cartridge.classList.contains("m_bottom-transform-active"))) {
    m_cartridge.classList.remove("m_top-transform-active");
    m_cartridge.classList.remove("m_bottom-transform-active");
  } else {
    window.demo_active = true;
    if (m_cartridge.classList.contains("top")) {
      m_cartridge.classList.add("m_top-transform-active");
    } else if (m_cartridge.classList.contains("bottom")) {
      m_cartridge.classList.add("m_bottom-transform-active");
    } else {
      console.log("mobile cartridge missing top or bottom class")
    };
    if (window.demo_active) {
      instruction = m_prompt_request_for[m_cartridge.id];
      window.last_instruction = instruction;
      // console.log("Requesting: " + instruction);
      window.current_worker = new_worker();
      // console.log("From Worker: " + current_worker);
      await demoSocket.send(JSON.stringify({
        "command": "stop",
        "worker": window.current_worker,
        'instruction': "die"
      }));
      await demoSocket.send(JSON.stringify({
        "command": "start",
        "worker": window.current_worker,
        'instruction': instruction
      }));
    } else {
      //pass
    }
  }
}


function new_worker() {
  var today = new Date();
  var time = today.getFullYear() + '-' + today.getMonth() + '-' + today.getDate() + "_" + today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
  var salt = randomString(length = 7);
  worker_name = time + salt;
  // console.log("Creating New Worker: " + worker_name);
  return worker_name;
};

function randomString(length = 16, chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ') {
  var result = '';
  for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
  return result;
};

//  Desktop Button Functions //
cartridge0.onclick = async function() {
  // console.log("clicked: " + cartridge0.id);
  select(cartridge0);
};

cartridge1.onclick = async function() {
  // console.log("clicked: " + cartridge1.id);
  select(cartridge1);
};

cartridge2.onclick = async function() {
  // console.log("clicked: " + cartridge2.id);
  select(cartridge2);
};

cartridge3.onclick = async function() {
  // console.log("clicked: " + cartridge3.id);
  select(cartridge3);
};

cartridge4.onclick = async function() {
  // console.log("clicked: " + cartridge4.id);
  select(cartridge4);
};

cartridge5.onclick = async function() {
  // console.log("clicked: " + cartridge5.id);
  select(cartridge5);
};

// Touch Button Functions //
m_cartridge0.ontouchstart = async function() {
  // console.log("clicked: " + cartridge0.id);
  m_select(m_cartridge0);
};

m_cartridge1.ontouchstart = async function() {
  // console.log("clicked: " + cartridge1.id);
  m_select(m_cartridge1);
};

m_cartridge2.ontouchstart = async function() {
  // console.log("clicked: " + cartridge2.id);
  m_select(m_cartridge2);
};

m_cartridge3.ontouchstart = async function() {
  // console.log("clicked: " + cartridge3.id);
  m_select(m_cartridge3);
};

m_cartridge4.ontouchstart = async function() {
  // console.log("clicked: " + cartridge4.id);
  m_select(m_cartridge4);
};

m_cartridge5.ontouchstart = async function() {
  // console.log("clicked: " + cartridge5.id);
  m_select(m_cartridge5);
};

// Rework Buttons //
rework_button.onmousedown = async function() {
  this.classList.add("clicked");
};

rework_button.onmouseup = async function() {
  this.classList.remove("clicked");
  // console.log("understood: " + cartridge.id);
  window.demo_active = false;
  window.manual_scrolling = false;
  await demoSocket.send(JSON.stringify({
    'command': "stop",
    "worker": window.current_worker,
    "instruction": "die"
  }));
  // console.log("Requesting: 'stop worker': " + current_worker);
  window.demo_active = true;
  if (window.demo_active) {
    instruction = window.last_instruction;
    window.last_instruction = instruction;
    // console.log("Requesting: " + instruction);
    window.current_worker = new_worker();
    // console.log("From Worker: " + current_worker);
    await demoSocket.send(JSON.stringify({
      "command": "stop",
      "worker": window.current_worker,
      'instruction': "die"
    }));
    await demoSocket.send(JSON.stringify({
      "command": "rework",
      "worker": window.current_worker,
      'instruction': instruction
    }));
  } else {
    //pass
  };
};

rework_button.onmouseleave = async function() {
  this.classList.remove("m_clicked");
};

m_rework_button.ontouchstart = async function() {
  if (window.supportsVibrate == true) {
    window.navigator.vibrate([20, 20, 50]);
  };
  this.classList.add("m_clicked");
  window.demo_active = false;
  window.manual_scrolling = false;
  await demoSocket.send(JSON.stringify({
    'command': "stop",
    "worker": window.current_worker,
    "instruction": "die"
  }));
  // console.log("Requesting: 'stop worker': " + current_worker);
  window.demo_active = true;
  if (window.demo_active) {
    instruction = window.last_instruction;
    window.last_instruction = instruction;
    // console.log("Requesting: " + instruction);
    window.current_worker = new_worker();
    // console.log("From Worker: " + current_worker);
    await demoSocket.send(JSON.stringify({
      "command": "stop",
      "worker": window.current_worker,
      'instruction': "die"
    }));
    await demoSocket.send(JSON.stringify({
      "command": "rework",
      "worker": window.current_worker,
      'instruction': instruction
    }));
  } else {
    //pass
  };
};

m_rework_button.ontouchend = async function() {
  this.classList.remove("m_clicked");
};

rework_button.ontouchcancel = async function() {
  this.classList.remove("m_clicked");
};
