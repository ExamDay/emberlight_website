from django.urls import path

from . import views

app_name = "main"

urlpatterns = [
    path("", views.home_page),
    path("robots.txt/", views.robots_txt),
    path("pricing/", views.pricing_page, name="pricing"),
    path("checkout/", views.checkout, name="checkout"),
    path("payment/", views.payment, name="payment"),
    path("payment/success", views.payment_success, name="payment_success"),
    path("contact/thanks/", views.contact_thanks),
    path("contact/", views.contact_page),
    path("support/", views.support_page),
    path("terms-of-service/", views.terms_of_service),
    path("privacy-policy/", views.privacy_policy),
    path("presskit/", views.presskit),
    path("ref/<str:coupon_id>/", views.referral_code),
    path("test/", views.test, name="test"),
]
