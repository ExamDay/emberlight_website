import json

from django.contrib.postgres.fields import JSONField
from django.db import models
from django.utils.safestring import mark_safe
from django.utils.timezone import now
from pygments import highlight
from pygments.formatters.html import HtmlFormatter
from pygments.lexers.data import JsonLexer


# from django.utils import timezone

# Create your models here.


class StripeCustomerData(models.Model):
    """Sets up the postgres table to keep track of Stripe information

    This class is used to keep track of the link between a customer's account on our system and
    the customer ID that is created for them in Stripe when they sign up for a subscription. That
    customer account is stored as a JSON and contains information like their hashed credit/debit
    card info, email, customer ID, time of creation, etc. The subscription that is linked to that
    customer ID is also stored on our server as a JSON and contains information like billing
    interval, plan ID, trial period information, latest invoice, cancel upon next billing cycle,
    etc.

    It should be noted that the creation of a customer ID and the creation of a subscription ID are
    two separate actions. The customer ID needs to be created first before a subscription ID can be
    attached to it. We do this in one function on our side for ease of use.

    Args:
        None

    Attributes:
        username (str) = models.CharField(max_length=200, unique=True)
            The username of the currently logged in user
        email = models.EmailField(default=0)
            The email of the currently logged in user
        subscription_active (bool) = models.BooleanField(default=False)
            Local check to see if subscription is active, value is set based on
            subscription_json["cancel_at_period_end"]
        subscription_start = models.DateTimeField(default=timezone.now())
            Provides an easy-to-view and easy-to-query field for when the subscription has begun.
        subscription_type = models.CharField(max_length=30, default=0)
            Provides an easy-to-view and easy-to-query field for what type of subscription the user
            has paid for.
        customer_json = JSONField()
            Contains a JSON from Stripe that contains the details of the user's customer ID that was
            created by Stripe.
        subscription_json = JSONField()
            Contains a JSON from Stripe that contains the details of the user's subscription ID that
            was created by Stripe.


    """

    username = models.CharField(max_length=200, unique=True)
    email = models.EmailField(default="None")
    subscription_active = models.BooleanField(default=False)
    subscription_start = models.DateTimeField(default=now, editable=False)
    subscription_type = models.CharField(max_length=30, default="None")
    customer_json = JSONField()
    subscription_json = JSONField()

    def customer_json_formatted(self):
        # with JSON field, no need to do .loads
        data = json.dumps(self.customer_json, indent=4)

        # format it with pygments and highlight it
        formatter = HtmlFormatter(style="colorful")
        response = highlight(data, JsonLexer(), formatter)

        # include the style sheet
        style = "<style>" + formatter.get_style_defs() + "</style>"

        return mark_safe(style + response)

    def subscription_json_formatted(self):
        # with JSON field, no need to do .loads
        data = json.dumps(self.subscription_json, indent=4)

        # format it with pygments and highlight it
        formatter = HtmlFormatter(style="colorful")
        response = highlight(data, JsonLexer(), formatter)

        # include the style sheet
        style = "<style>" + formatter.get_style_defs() + "</style>"

        return mark_safe(style + response)

    customer_json_formatted.short_description = "Customer Data Formatted"
    subscription_json_formatted.short_description = "Subscription Data Formatted"

    class Meta:
        verbose_name = "Stripe Customer Data"
        verbose_name_plural = "Stripe Customers Data"

    def __str__(self):
        return self.username


class MiscUserInfo(models.Model):
    username = models.CharField(max_length=200, unique=True)
    email = models.EmailField(default="None")
    email_list = models.BooleanField(default=True)
    pre_alpha = models.BooleanField(default=False)
    alpha = models.BooleanField(default=False)
    beta = models.BooleanField(default=False)
    indiegogo_backer = models.BooleanField(default=False)
    account_coupon = models.CharField(max_length=200)
    media_account = models.BooleanField(default=False)
    unix_timestamp_temp_access_start = models.IntegerField(default=0)
    allow_temporary_access = models.BooleanField(default=False)
