from decouple import config
from django.conf import settings
from django.http import HttpResponseNotFound
from django.http import HttpResponseRedirect


class EnforceGlobalLoginMiddleware:
    """
    Middlware class which requires the user to be authenticated for all urls except those defined in
    TEST_SITE_PUBLIC_URLS in settings.py. TEST_SITE_PUBLIC_URLS should be a list of strings for the
    urls you want anonymous users to have access to. Requests for urls not matching
    TEST_SITE_PUBLIC_URLS get redirected to LOGIN_URL with next set to original path of the
    unauthenticted request. Any urls statically served by django are excluded from this check. To
    enforce the samevalidation on these set SERVE_STATIC_TO_PUBLIC to False.
    """

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        """
        Redirect anonymous users to login_url from non public urls
        """
        response = self.get_response(request)
        requested_url = str(request.path[1:])
        if settings.DEBUG and config("WEBSITE_ENV") == "production":
            if (requested_url not in settings.TEST_SITE_PUBLIC_URLS) and (
                request.user.is_anonymous
            ):
                return HttpResponseRedirect(settings.LOGIN_URL)
            elif not request.user.is_superuser and request.user.is_authenticated:
                return HttpResponseNotFound("You are not authorized to view this content.")
            else:
                return response
        if settings.DEBUG and config("WEBSITE_ENV") == "development":
            return response
        elif not settings.DEBUG:
            return response
