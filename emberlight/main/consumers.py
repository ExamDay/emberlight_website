import asyncio
import json
from random import randint

from channels.generic.websocket import AsyncWebsocketConsumer

from .static.main.misc.demo_text.bitcoin import bitcoin
from .static.main.misc.demo_text.coronavirus import coronavirus
from .static.main.misc.demo_text.createyourown import createyourown
from .static.main.misc.demo_text.facialrecognition import facialrecognition
from .static.main.misc.demo_text.nuclearenergy import nuclearenergy
from .static.main.misc.demo_text.space import space

# Importing Demo Output Lists:
# Channels Stuff:


bitcoin_prompt = "Bitcoin offers certain advatages over traditional currencies. Users should be wary about digital currency's benefits and risks seeing as they are a relatively new and untested medium. That said, Bitcoin appears to offer some unique possibilities."
facialrecognition_prompt = "Computers are getting better and better at recognizing people's faces. As more and more survelance technologies become integrated into society, it brings up the question of where does it end? Should facial recognition technology be used in schools?"
nuclearenergy_prompt = "Nuclear fission represents perhaps the most abundant source of energy available to humanity. Unfortunately, it is also very easy to get wrong. While the basic concepts are simple, practical application of the technology is rife with little subtleties which, if not considered responsibly, can produce hazardous and expensive complications. For example,"
coronavirus_prompt = "As cases of the new coronavirus (COVID-19) soar in Europe and the Middle East, and infections crop up in several additional countries — cumulatively at least 48 so far — the signs have been everywhere since Thursday that the epidemic shaking much of the world is only getting started. The illness, first detected in China in December, appears to be entering a troubling new phase."
space_prompt = "Human space exploration helps to address fundamental questions about our place in the Universe and the history of our solar system. Through addressing the challenges related to human space exploration we expand technology, create new industries, and help to foster a peaceful connection with other nations."
createyourown_prompt = "Hey, this is really cool! Can I try one?"

demo_options = {
    "bitcoin": bitcoin,
    "facialrecognition": facialrecognition,
    "nuclearenergy": nuclearenergy,
    "coronavirus": coronavirus,
    "space": space,
    "createyourown": createyourown,
}

blurbs = {
    "bitcoin": bitcoin_prompt,
    "facialrecognition": facialrecognition_prompt,
    "nuclearenergy": nuclearenergy_prompt,
    "coronavirus": coronavirus_prompt,
    "space": space_prompt,
    "createyourown": createyourown_prompt,
}


class demoConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.workers = {"none": True}
        # print("connected")
        await self.accept()

    async def disconnect(self, close_code):
        # stop all jobs on disconnect
        # print("disconnected")
        pass

    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        print("\nMessage Recieved: " + str(text_data_json))
        command = text_data_json["command"]
        worker = text_data_json["worker"]
        instruction = text_data_json["instruction"]
        # print("Command: " + command)
        # print("Worker: " + worker)
        # print("Instuction: " + instruction)
        if command == "start":
            self.workers[worker] = True
            selection = instruction
            if selection in demo_options:
                flavorselection = demo_options[selection]
                sample = flavorselection[randint(0, len(flavorselection) - 1)]
                elaboration = tuple(sample)
                asyncio.ensure_future(
                    self.demo_elaboration(blurb=selection, elaboration=elaboration, worker=worker)
                )
            else:
                print("Invalid Selection: " + selection)
                pass
        elif command == "rework":
            self.workers[worker] = True
            selection = instruction
            if selection in demo_options:
                flavorselection = demo_options[selection]
                sample = flavorselection[randint(0, len(flavorselection) - 1)]
                elaboration = tuple(sample)
                asyncio.ensure_future(
                    self.demo_elaboration_rework(
                        blurb=selection, elaboration=elaboration, worker=worker
                    )
                )
            else:
                print("Invalid Selection: " + selection)
                pass
        elif command == "stop":
            try:
                del self.workers[worker]
            except KeyError:
                # print("Worker number: " + worker + " cannot be deleted because it does not exist.")
                pass
            finally:
                pass
        else:
            print("Unknown Command: " + command)
            pass

    async def demo_elaboration(self, blurb, elaboration, worker):

        await self.send(text_data=json.dumps({"command": "clear", "instruction": "none"}))

        await asyncio.sleep(0.5)
        if worker in self.workers:
            await self.send(
                text_data=json.dumps(
                    {
                        "command": "write",
                        "instruction": "<font size=6px><strong>Prompt:</strong></font><br><br>",
                    }
                )
            )
        else:
            pass

        await asyncio.sleep(0.5)
        if worker in self.workers:
            chunk_size = int(25)
            async for chunk in self.emberlightgen(
                chunk_size=chunk_size, delay=0.005, textlist=blurbs[blurb]
            ):
                if worker in self.workers:
                    # print(letter)
                    await self.send(
                        text_data=json.dumps({"command": "write", "instruction": chunk})
                    )
                else:
                    break

        else:
            pass

        await asyncio.sleep(0.5)
        if worker in self.workers:
            await self.send(
                text_data=json.dumps(
                    {
                        "command": "write",
                        "instruction": "<br><br><font size=6px><strong>Creation:  </strong></font>",
                    }
                )
            )
        else:
            pass

        loadtime = 2.5
        hashtags = 7
        async for i in self.aiter(iters=hashtags):
            await asyncio.sleep(loadtime / hashtags)
            if worker in self.workers:
                await self.send(
                    text_data=json.dumps(
                        {
                            "command": "write",
                            "instruction": "<font size=6px><strong>#</strong></font>",
                        }
                    )
                )
            else:
                break

        if worker in self.workers:
            await self.send(text_data=json.dumps({"command": "write", "instruction": "<br><br>"}))
        else:
            pass

        async for letter in self.emberlightgen(chunk_size=1, delay=0.015, textlist=elaboration):
            if worker in self.workers:
                # print(letter)
                # letter = ["<br>" if x=="\n" else x for x in letter]
                await self.send(text_data=json.dumps({"command": "write", "instruction": letter}))
            else:
                break

    async def demo_elaboration_rework(self, blurb, elaboration, worker):

        await self.send(text_data=json.dumps({"command": "clear", "instruction": "none"}))
        if worker in self.workers:
            await self.send(
                text_data=json.dumps(
                    {
                        "command": "write",
                        "instruction": "<font size=6px><strong>Prompt:</strong></font><br><br>"
                        + blurbs[blurb]
                        + "<br><br><font size=6px><strong>Creation:  </strong></font>",
                    }
                )
            )
        else:
            pass

        loadtime = 2.5
        hashtags = 7
        async for i in self.aiter(iters=hashtags):
            await asyncio.sleep(loadtime / hashtags)
            if worker in self.workers:
                await self.send(
                    text_data=json.dumps(
                        {
                            "command": "write",
                            "instruction": "<font size=6px><strong>#</strong></font>",
                        }
                    )
                )
            else:
                break

        if worker in self.workers:
            await self.send(text_data=json.dumps({"command": "write", "instruction": "<br><br>"}))
        else:
            pass

        async for letter in self.emberlightgen(chunk_size=1, delay=0.015, textlist=elaboration):
            if worker in self.workers:
                # print(letter)
                # letter = ["<br>" if x=="\n" else x for x in letter]
                await self.send(text_data=json.dumps({"command": "write", "instruction": letter}))
            else:
                break

    async def emberlightgen(self, chunk_size, delay, textlist):
        remainder = len(textlist) % chunk_size
        for chunk in range((len(textlist) // chunk_size) + 1):
            await asyncio.sleep(delay)
            try:
                letters = textlist[chunk_size * chunk : (chunk_size * chunk) + chunk_size]
                "".join(letters)
                yield letters
            except IndexError:
                letters = textlist[chunk_size * chunk : (chunk_size * chunk) + remainder]
                yield letters
            finally:
                pass

    async def aiter(self, iters):
        for number in range(iters):
            yield number
