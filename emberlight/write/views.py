import jwt
from account.models import StripeCustomerData
from decouple import config
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from django.shortcuts import render

# Create your views here.


@login_required
def write_page(request):
    title = "Emberlight - Write"
    token = jwt.encode(
        {"username": request.user.username}, config("DJANGO_WEBSOCKET_SECRET"), algorithm="HS256"
    ).decode("utf-8")
    context = {"title": title, "token": token}

    # Allow superusers free access
    if request.user.is_superuser:
        return render(request, "write/write.html", context)

    # -- Verify (Active) Payment -- #
    try:
        subscription_status = (
            StripeCustomerData.objects.all().get(username=request.user).subscription_json["status"]
        )
    except StripeCustomerData.DoesNotExist:
        messages.error(request, "You need a subscription to access the write page!")
        title = "Emberlight - Pricing"
        context = {"title": title, "key": settings.STRIPE_PUBLISHABLE_KEY}
        return redirect("/pricing", context)

    if str(request.user) not in tuple(
        (x[0] for x in tuple(StripeCustomerData.objects.all().values_list("username")))
    ):
        # if the user hasn't paid for a subscription
        title = "Emberlight - Pricing"
        context = {"title": title, "key": settings.STRIPE_PUBLISHABLE_KEY}
        return redirect("/pricing", context)
    elif subscription_status != "active" and subscription_status != "trialing":
        # if the user doesn't have an active subscription
        messages.error(request, "In order to access this page, you need to resubscribe!")
        title = "Emberlight - Account"
        context = {"title": title, "key": settings.STRIPE_PUBLISHABLE_KEY}
        return redirect("/account", context)

    return render(request, "write/write.html", context)
