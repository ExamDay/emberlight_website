from django.urls import path

from . import consumers

websocket_urlpatterns = [
    path("ws/write/", consumers.writeConsumer),
    path("ws/ai/", consumers.AIConsumer),
]
