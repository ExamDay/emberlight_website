import traceback

import jwt
import nltk
from decouple import config

try:  # get natural language processing prereqs
    nltk.download("punkt")  # this sticks around permanently on the system.
except Exception:  # so we dont have to worry about it not updating sometimes.
    pass


def count_sentences(text: str):
    split_text = nltk.tokenize.sent_tokenize(text)
    return len(split_text)


def verify_client_message(message):
    # -- Data verification -- #
    if not isinstance(message, dict):
        error_message = "Verification Failed: message body not in dict format."
        print(error_message)
        return error_message

    for element in ["token", "username", "command", "worker"]:
        if element not in message:
            error_message = "Failed: '{}' not in message dict.".format(element)
            print(error_message)
            return error_message

    # -- Verify the token -- #
    try:
        jwt.decode(message["token"], config("DJANGO_WEBSOCKET_SECRET"), algorithms="HS256")

    except Exception:
        print("Failed: Token verification failed.")
        return traceback.format_exc()
    # -- return VERIFIED only if all tests passed -- #
    return "VERIFIED"


def verify_elaborate(message):
    instructions = message["info"]
    try:
        assert 0 < instructions["length"] <= 500
        assert instructions["nsamples"] == 1
        assert instructions["batch_size"] == 1
        assert instructions["top_k"] <= 100
        assert instructions["sentences"] < 2000
    except AssertionError:
        print("\nBad instructions for elaborate command.\n")
        traceback.print_exc()
        return traceback.format_exc()
    return "VERIFIED"


def verify_summarize(message):
    instructions = message["info"]
    try:
        assert 0 < instructions["length"] <= 500
        assert instructions["nsamples"] == 1
        assert instructions["batch_size"] == 1
        assert instructions["top_k"] <= 50
        assert instructions["sentences"] <= 25
    except AssertionError:
        print("\nBad instructions for summarize command.\n")
        traceback.print_exc()
        return traceback.format_exc()
    return "VERIFIED"


def verify_start_command(message):
    # -- Verify start instructions from client are within allowed limits -- #
    known_start_commands = {"elaborate": verify_elaborate, "summarize": verify_summarize}
    verify_command = known_start_commands[message["command"]]
    return verify_command(message)


def elaborate_bounds(message):
    return "IN_BOUNDS"


def summarize_bounds(message):
    try:
        assert len(message["info"]["prompt"]) > 0
    except AssertionError:
        return ""
    try:
        assert count_sentences(message["info"]["prompt"]) > 1  # do not accept empty prompts
    except AssertionError:
        return "<strong style='font-family: mono;'>[NOTICE: more than one sentence must be supplied for a summary.]</strong>"
    return "IN_BOUNDS"


def start_command_bounds(message):
    # -- Verify start instructions from client are within allowed limits -- #
    known_start_commands = {"elaborate": elaborate_bounds, "summarize": summarize_bounds}
    constrain_command = known_start_commands[message["command"]]
    return constrain_command(message)


def verify_server_signal(signal):
    # -- Verify signals come from proported source -- #
    key = signal["key"]
    try:
        assert key == config("INTRASERVER_COMMS_KEY")
    except AssertionError:
        print("\nBad intraserver comms key in signal.\n")
        traceback.print_exc()
        return traceback.format_exc()
    return "VERIFIED"
