import asyncio
import datetime
import json
import logging
import traceback
from html.parser import HTMLParser
from random import choice
from time import time

import boto3
import redis
from channels.exceptions import StopConsumer
from channels.generic.websocket import AsyncWebsocketConsumer
from decouple import config
from django.conf import settings
from django.core.mail import send_mail

from .security import start_command_bounds
from .security import verify_client_message
from .security import verify_server_signal
from .security import verify_start_command

# logging
env = config("WEBSITE_ENV")
if env == "development":
    logging.basicConfig(
        filename="write_consumers.log",
        level=logging.DEBUG,
        format="[%(asctime)s|%(name)s|write/consumers.py|%(levelname)s] %(message)s",
    )
elif env == "production":
    logging.basicConfig(
        filename="/webapps/emberlight/logs/gunicorn_supervisor.log",
        level=logging.INFO,
        format="[%(asctime)s|%(name)s|write/consumers.py|%(levelname)s] %(message)s",
    )

# database
rdb = redis.Redis(
    host="localhost", port=6379, db=0, password=config("REDIS_PASSWORD"), decode_responses=True
)

# clean up after crash/reboot
pipe = rdb.pipeline()
pipe.delete("load_history")
pipe.delete("request_history")
pipe.delete("connection_history")
pipe.delete("ai_lrt")
pipe.delete("ai_channels")
pipe.delete("active_instances")
pipe.delete("instance_channel_map")
pipe.delete("connections")
pipe.delete("clients")
pipe.delete("client_connections")
pipe.execute()

# AWS
ec2 = boto3.Session(profile_name="emberlight-compute-manager").resource("ec2")
client = boto3.Session(profile_name="emberlight-compute-manager").client("ec2")


# resource and database management:
def utc_now():
    return datetime.datetime.now(datetime.timezone.utc).timestamp()


webserver_launch_time = time()


class html_to_python(HTMLParser):

    tag_dict = {"br": "\n"}
    translated_text = ""

    def handle_starttag(self, tag, attrs):
        try:
            self.translated_text += self.tag_dict[tag]
        except KeyError:
            pass

    def handle_endtag(self, tag):
        pass

    def handle_data(self, data):
        self.translated_text += data


class writeConsumer(AsyncWebsocketConsumer):
    """consumer for websocket connections on /ws/write (handles requests from clients on
    write page)"""

    global rdb
    global ec2
    global html_to_python

    async def connect(self, database=rdb):
        """triggers on connection to /ws/ai"""
        # TODO Validate tokens in URL upon connection

        # ## VALIDATION TEMPLATE RIPPED FROM LAMBDA ###
        # if event["requestContext"]["eventType"] == "CONNECT":
        #     print("Connect requested")

        #     # Ensure token was provided
        #     if not is_ai:
        #         if not token:
        #             print("Failed: token query parameter not provided.")
        #             return _get_response(400, "token query parameter not provided.")

        #         # Verify the token
        #         try:
        #             payload = jwt.decode(token, os.environ["SECRET"], algorithms="HS256")
        #             print("Verified JWT for '{}'".format(payload.get("username")))
        #         except Exception:
        #             print("Failed: Token verification failed.")
        #             return

        #     # Add ConnectionID to the database
        #     table = dynamodb.Table("emberlight_Connections")
        #     table.put_item(
        #         Item={
        #             "ConnectionID": ConnectionID,
        #             "is_ai": True if is_ai else False,
        #             "available": True if is_ai else False,
        #         }
        #     )
        #     return _get_response(200, "Connect Successful")

        # elif event["requestContext"]["eventType"] == "DISCONNECT":
        #     print("Disconnect requested")

        # else:
        #     print("Connection manager received unrecognized eventType.")
        # ## END VALIDATION TEMPLATE ###
        await self.accept()
        self.signed_in = False
        self.worker = "indeterminate"
        # Adding connection to DB, checking for available AI, and getting current number of
        # connections:
        pipe = database.pipeline()
        pipe.zadd("connections", {self.channel_name: time()})
        pipe.zcard("connections")
        pipe_return = pipe.execute()
        # print("\nAdded Client Channel Name To Redis DB\n")
        # Recording New Connections State in Database:
        database.zadd("connection_history", {self.mark_and_json(data=pipe_return[1]): time()})

    async def disconnect(self, close_code, database=rdb):
        """triggers on loss of signal or intentional disconnection"""
        pipe = database.pipeline()
        pipe.zrem("connections", self.channel_name)
        pipe.zrem("clients", self.username)
        pipe.zcard("connections")
        pipe_return = pipe.execute()
        database.zadd("connection_history", {self.mark_and_json(data=pipe_return[2]): time()})
        message = {"command": "stop", "worker": self.worker}
        await self.stop_worker(message=message)
        # print("Client on Write Page Disconnected")

    async def sound_alarm(self, warning):
        """logs information about events that may indicate clientside malicious behaviour or
        service abuse. Threatens the attacker with a "HACKER ALERT" style message when applicable,
        and emails development and security teams with information about the incident."""
        message = "\n\nTHREAT: ORIGIN FROM USER: " + self.username + "\n\n" + warning + "\n"
        # record incident
        logging.critical("\n\n" + message)
        # alert security detail
        send_mail(
            "SECURITY THREAT",
            message,
            settings.EMAIL_HOST_USER,
            ["engineering@torchlightintel.com"],
        )
        # send hacker alert
        payload = {
            "command": "alert",
            "info": "MALICIOUS BEHAVIOUR DETECTED: THIS INCIDENT HAS BEEN REPORTED.",
        }
        await self.send(text_data=json.dumps(payload))
        # drop connection
        await self.disconnect()

    async def receive(self, text_data, database=rdb):
        """receptor for messages from connected client. Triggers on every message recieved over
        write page websocket connection."""
        known_commands = {
            "summarize": self.start_worker,
            "elaborate": self.start_worker,
            "stop": self.stop_worker,
            "sign_in": self.sign_in,
        }
        message = json.loads(text_data)
        verification_status = verify_client_message(message)
        if verification_status == "VERIFIED":
            try:
                command = known_commands[message["command"]]
                await command(message)
            except KeyError:
                payload = {
                    "command": "alert",
                    "info": "Recieved unrecognized or unauthorized command -- this incident has"
                    " been reported.",
                }
                await self.sound_alarm(
                    "Recieved unrecognized or unauthorized command: {}\nFrom User: {}".format(
                        command, self.username
                    )
                )
                await self.send(text_data=json.dumps(payload))
            finally:
                pass
        else:
            await self.sound_alarm(verification_status)
        database.zadd("request_history", {self.mark_and_json(data=self.username): time()})

    def mark_and_json(
        self, data, length=8, chars="0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
    ):
        """a tool for storing python objects as byte strings in Redis
        takes some [data], a [length] for the unique id string and
        returns a tuple like: (unique_id, json_encoded_[data])"""
        random_string = ""
        for i in range(length):
            random_string += choice(tuple(chars))
        unique_tuple = (data, random_string)
        return json.dumps(unique_tuple, ensure_ascii=False).encode("utf8")

    async def stop_worker(self, message, database=rdb):
        """gracefully stops AI worker associated with this consumer"""
        try:
            ai = self.ai_channel[0]
            message["type"] = "client.order"
            while True:
                try:
                    await self.channel_layer.send(ai, message)
                    break
                except Exception:
                    await asyncio.sleep(0.05)
                    continue
        except Exception:
            pass

    async def start_worker(self, message, database=rdb):
        """starts AI worker with attributes and job described in [message]"""
        verification_status = verify_start_command(message)
        if self.signed_in and (verification_status == "VERIFIED"):
            check = start_command_bounds(message)
            try:
                assert check == "IN_BOUNDS"
            except AssertionError:
                payload = {
                    "command": ["write"],
                    "meta": {"timestamp": utc_now()},
                    "info": {"elab_fragment": check},
                    "routing": message["routing"],
                }
                print(payload)
                await self.send(json.dumps(payload))  # send check message to client
                return  # do nothing if command out of bounds
            # enforce one job per client connection at a time:
            await self.stop_worker(message={"command": "stop", "worker": self.worker})
            # start new job
            payload = message
            payload["type"] = "client.order"
            if "routing" in payload:
                payload["routing"]["cid"] = self.channel_name
                payload["routing"]["user"] = "rando"
                payload["routing"]["domain"] = "it's me"
                payload["routing"]["stage"] = "the world"
            else:
                payload["routing"] = {
                    "cid": self.channel_name,
                    "user": "rando",
                    "domain": "it's me.",
                    "stage": "the world",
                }
            text = payload["info"]["prompt"]
            # -- truncating text -- #
            if len(text) > 2000:
                text = text[-2000::]
            # -- parsing html from text -- #
            parser = html_to_python()
            parser.feed(text)
            allowed_text = parser.translated_text
            # updating prompt
            payload["info"]["prompt"] = allowed_text
            if database.exists("ai_channels") == 1:  # 1 means it exists, 0 means it doesn't.
                self.ai_channel = database.zpopmin("ai_channels")[
                    0
                ]  # even though it's only one tuple we are requesting, redis still encapsulates it
                # in a list.
                while True:
                    try:  # check for backpressure. If stack overloaded, wait for it to empty
                        # and try again.
                        await self.channel_layer.send(self.ai_channel[0], payload)
                        database.zadd("ai_channels", {self.ai_channel[0]: self.ai_channel[1] + 11})
                        break
                    except Exception:
                        await asyncio.sleep(0.05)
                        continue
                self.worker = message["worker"]
        else:
            if not self.signed_in:
                logging.error(
                    "\nTHREAT: Someone is tinkering with the javascript on their end, trying to"
                    ' issue commands without sending a "sign_in" command first. This would prevent'
                    " us from logging who the commands are coming from.\n"
                )
            await self.sound_alarm(verification_status)

    async def sign_in(self, message, database=rdb):
        """records authenticaion of person with information provided in [message].
        this is done only after a "sign_in" request from a valid user has been
        authenticated by security.verify_client_message()."""
        self.username = message["username"]
        database.zadd("clients", {self.username: time()})
        # -- enforce one connection per client -- #
        preexisting_channel = database.hget("client_connections", self.username)
        if preexisting_channel is not None:
            try:
                # closes the old connection with a disconnect signal
                payload = {
                    "type": "server.signal",
                    "key": config("INTRASERVER_COMMS_KEY"),
                    "command": "enforce_single_session",
                }
                await self.channel_layer.send(preexisting_channel, payload)
            except Exception:
                pass
        # -- register new connection for the client --  #
        database.hset("client_connections", self.username, self.channel_name)
        self.signed_in = True

    async def server_signal(self, event):
        """receptor for all intraserver signals to this process. (triggers on all messages of
        type = "server.signal" on self.channel.)"""
        known_commands = {"enforce_single_session": self.enforce_single_session}
        verification_status = verify_server_signal(event)
        if verification_status == "VERIFIED":
            try:
                command = known_commands[event["command"]]
                await command()
            except KeyError:
                logging.critical(
                    "HIGH LEVEL THREAT: UNRECOGNIZED COMMAND FROM (SUPPOSED) SERVER! SECRET KEY"
                    " LIKELY COMPROMISED! THIS DEMANDS IMMEDIATE ACTION!\nABERRANT COMMAND: ",
                    event["command"],
                )
                await self.disconnect()
        else:
            await self.sound_alarm(verification_status)

    async def enforce_single_session(self):
        """kills the older connection if another connection past the first one is opened"""
        # send closed alert to old connection before closing it (alert appears on first
        # device/browser/tab to inform user that it has closed)
        payload = {"command": "alert", "info": "Service open on another device, browser, or tab."}
        await self.send(text_data=json.dumps(payload))
        await self.disconnect()

    async def ai_message(self, event):
        """receptor for messages from AI consumers. (triggers on all messages of
        type = "ai.message" on self.channel.)"""
        try:
            del event["type"]
        except Exception:
            logging.error(traceback.format_exc())
            pass
        await self.send(json.dumps(event))


class AIConsumer(AsyncWebsocketConsumer):
    """consumer for websocket connections on /ws/ai"""

    global rdb
    global client

    async def connect(self):
        """triggers on connection to /ws/ai"""
        # ## VALIDATION TEMPLATE RIPPED FROM LAMBDA ###
        # if event["requestContext"]["eventType"] == "CONNECT":
        #     print("Connect requested")

        #     # Ensure token was provided
        #     if not is_ai:
        #         if not token:
        #             print("Failed: token query parameter not provided.")
        #             return _get_response(400, "token query parameter not provided.")

        #         # Verify the token
        #         try:
        #             payload = jwt.decode(token, os.environ["SECRET"], algorithms="HS256")
        #             print("Verified JWT for '{}'".format(payload.get("username")))
        #         except Exception:
        #             print("Failed: Token verification failed.")
        #             return

        #     # Add ConnectionID to the database
        #     table = dynamodb.Table("emberlight_Connections")
        #     table.put_item(
        #         Item={
        #             "ConnectionID": ConnectionID,
        #             "is_ai": True if is_ai else False,
        #             "available": True if is_ai else False,
        #         }
        #     )
        #     return _get_response(200, "Connect Successful")

        # elif event["requestContext"]["eventType"] == "DISCONNECT":
        #     print("Disconnect requested")

        # else:
        #     print("Connection manager received unrecognized eventType.")
        # ## END VALIDATION TEMPLATE ###

        # accepting connection
        self.instance_id = "indeterminate"
        await self.accept()
        # asyncio.ensure_future(self.meseeks_protocol())

    async def disconnect(self, close_code, database=rdb):
        """triggers on loss of signal or intentional disconnection"""
        print("AI Disconnected")
        logging.info("AI Disconnected")
        await self.sepuku()

    async def receive(self, text_data):
        """receptor for messages from connected AI. Triggers on every message recieved over
        AI websocket connection."""
        known_functions = {
            "write": self.write,
            "report_in": self.report_in,
            "conscript": self.conscript,
            "sepuku": self.sepuku,
        }
        message = json.loads(text_data)
        for function_string in message["functions"]:
            try:
                consumer_function = known_functions[function_string]
                await consumer_function(message=message)
            except KeyError:
                logging.critical(
                    "\nRecieved Unrecognized Function From Supposed AI\n", traceback.format_exc()
                )
                self.close()
                pass

    async def client_order(self, event):
        """receptor for messages from client consumers. (triggers on all messages of
        type = "client.order" on self.channel.)"""
        await self.send(text_data=json.dumps(event))

    async def conscript(self, message, database=rdb):
        """Called upon AI's first connection. Declares all useful attributes, populates database
        with appropriate records of conscription, makes AI available for dispatch, and starts
        keepalive-timer/death-clock."""
        print("\nconscripting...")
        logging.info("\nconscripting...")
        # pledging mind and body to the Glory of the Holy Torchlight Empire.
        self.instance_id = message["info"]["instance_id"]
        # finding place in barracks:
        pipe = database.pipeline()
        pipe.zadd("ai_lrt", {self.instance_id: time()})
        pipe.hset("instance_channel_map", self.instance_id, self.channel_name)
        # Adding name to book of living:
        pipe.zadd("ai_channels", {self.channel_name: 0})
        # add instance to active_instances with it's launch time from AWS.
        if self.instance_id[1] == "i":
            try:
                launch_time = (
                    client.describe_instances(InstanceIds=[self.instance_id])["Reservations"][0][
                        "Instances"
                    ][0]["LaunchTime"]
                ).timestamp()
            except Exception:
                launch_time = time()
        else:
            launch_time = time()
        pipe.zadd("active_instances", {self.instance_id: launch_time})
        pipe.zrem("inbound_instances", self.instance_id)
        pipe.execute()
        print("Conscripted: " + self.instance_id + " into Holy Robot Army.\n")
        logging.info("Conscripted: " + self.instance_id + " into Holy Robot Army.\n")
        # print("\nAIs: " + str(database.zrange("ai_channels", 0, 100)))

    async def report_in(self, message, database=rdb):
        """AI report load status and hit keepalive button (heartbeat)"""
        pipe = database.pipeline()
        # Reseting death clock:
        pipe.zadd("ai_lrt", {self.instance_id: time()})
        # Updating load status in book of living.
        load = message["status"]["load"]
        pipe.zadd("ai_channels", {self.channel_name: load})
        pipe.execute()

    async def write(self, message):
        """sends writen text from ai to consumer of the client who requested it"""
        payload = {
            "type": "ai.message",
            "command": "write",
            "info": message["info"],
            "routing": {"stage": message["routing"]["stage"]},
        }
        target_consumer = message["routing"]["cid"]
        while True:
            try:
                await self.channel_layer.send(target_consumer, payload)
                break
            except Exception:
                await asyncio.sleep(0.05)
                continue

    # async def meseeks_protocol(self, period=500, database=rdb):
    #     while True:
    #         await asyncio.sleep(period)
    #         if database.exists(self.channel_name) == 0:
    #             print("Oooh weeee!")
    #             await self.sepuku()
    #             break

    async def sepuku(self, message=None, database=rdb):
        """exit and delete self gracefully"""
        print("AI Consumer Sepuku")
        logging.info("AI Consumer Sepuku")
        pipe = database.pipeline()
        pipe.zrem("ai_channels", self.channel_name)
        pipe.hdel("instance_channel_map", self.instance_id)
        pipe.execute()
        raise StopConsumer()
