// Start Browser Detection by Duck Typing:
// Opera 8.0+
var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;

// Firefox 1.0+
var isFirefox = typeof InstallTrigger !== 'undefined';

// Safari 3.0+ "[object HTMLElementConstructor]"
var isSafari = /constructor/i.test(window.HTMLElement) || (function(p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification));

// Internet Explorer 6-11
var isIE = /*@cc_on!@*/ false || !!document.documentMode;

// Edge 20+
var isEdge = !isIE && !!window.StyleMedia;

// Chrome 1 - 79
var isChrome = !!window.chrome && (!!window.chrome.webstore || !!window.chrome.runtime);

// Edge (based on chromium) detection
var isEdgeChromium = isChrome && (navigator.userAgent.indexOf("Edg") != -1);

// Blink engine detection
var isBlink = (isChrome || isOpera) && !!window.CSS;


var output = 'Browsers Detected:\n';
output += 'isFirefox: ' + isFirefox + '\n';
output += 'isChrome: ' + isChrome + '\n';
output += 'isSafari: ' + isSafari + '\n';
output += 'isOpera: ' + isOpera + '\n';
output += 'isIE: ' + isIE + '\n';
output += 'isEdge: ' + isEdge + '\n';
output += 'isEdgeChromium: ' + isEdgeChromium + '\n';
output += 'isBlink: ' + isBlink + '\n';
console.log(output);
// End Browser Detection

var icons = Quill.import('ui/icons');
icons['elaborate'] = elaboration_button;
icons['summarize'] = summarization_button;
icons['articulate'] = connection_button;

var toolbarOptions = [
  ['elaborate'],
  ['summarize'],
  ['articulate'],
  [{ 'size': ['small', false, 'large', 'huge'] },
    { 'font': [] },
    { 'color': [] }, { 'background': [] }, // dropdown with defaults from theme
    'bold', 'italic', 'underline', 'strike'
  ], // toggled buttons
  [{ 'script': 'sub' }, { 'script': 'super' }], // superscript/subscript
  ['blockquote'],
  // ['video', 'image', 'code-block'],
  [{ 'list': 'ordered' }, { 'list': 'bullet' }],
  [{ 'indent': '-1' }, { 'indent': '+1' }, // outdent/indent
    { 'align': [] },
    { 'direction': 'rtl' }
  ], // text direction
  // ['clean'] // remove formatting button
];

var quill = new Quill('#editor', {
  theme: 'snow',
  modules: {
    toolbar: toolbarOptions
  },
  placeholder: "\nHere is an editor with some text formatting tools. Notice the new icons at the top left. These are tools that you can use to control the AI. Click the \"Elaborate\" button on the far left to send everything in this field to the AI. Alternatively, you can highlight the text you want to send, and then right-click or hit Crtl+e."
  // formula: true          // Include formula module (needs extra css + Katex Script)
});

quill.root.setAttribute('data-gramm', 'false');
if (isChrome || isEdgeChromium) {
  quill.root.setAttribute('spellcheck', 'false');
} else {
  quill.root.setAttribute('spellcheck', 'true');
};

var elaborateButton = document.querySelector('.ql-elaborate');
var summarizeButton = document.querySelector('.ql-summarize');
var articulateButton = document.querySelector('.ql-articulate');
var suggestion_output_field = document.getElementById("suggestion_output_field")

var app_active = true;
var current_worker = "zero";
var last_prompt = "";
var writeSocket;

// set toolbar-editor overlap and top padding of editor to match height of toolbar
function ocd_protocol() {
  var toolbar_height = document.getElementsByClassName("ql-toolbar").item(0).clientHeight
  editor = document.getElementById("editor")
  editor.style.marginTop = (-toolbar_height).toString() + "px";
  editor.style.paddingTop = toolbar_height.toString() + "px";
};

ocd_protocol();

window.onresize = async function() {
  ocd_protocol();
};

// shorten navbar color-on-scroll threshold //
document.getElementById("sectionsNav").setAttribute("color-on-scroll", "25");

// Connection Code //
function setupWebSocket(username, token) {
  var loc = window.location;
  var wsStart = "ws://";
  if (loc.protocol == "https:") {
    var wsStart = "wss://"
  };
  var endpoint = wsStart + loc.host + '/ws/write/';
  console.log("endpoint: " + endpoint)
  window.writeSocket = new WebSocket(endpoint);

  window.writeSocket.onmessage = async function(e) {
    // console.log("output recieved", e)
    var data = JSON.parse(e.data);
    var command = data['command'];

    if (command == "write") {
      if (window.app_active) {
        var output = data['info']["elab_fragment"];
        var unformed_text = window.suggestion_output_field.innerHTML + output;
        window.suggestion_output_field.innerHTML = unformed_text.replace("\n", "<br>");
      } else {
        //pass
      }
    } else if (command == "clear") {
      window.suggestion_output_field.innerHTML = ("");
    } else if (command == "alert") {
      window.alert(data["info"]);
    } else {
      // console.log("Unknown Command: " + command)
    }
    autoScroll(suggestion_output_field, 15);
  };

  window.writeSocket.onclose = async function(e) {
    console.log('write socket closed unexpectedly', e);
  };

  window.writeSocket.onopen = async function(e) {
    console.log('open', e);
    await this.send(JSON.stringify({
      "username": username,
      "token": token,
      "command": "sign_in",
      "worker": "none",
    }));
  };

  window.writeSocket.onerror = async function(e) {
    console.log('error', e);
  };
}

setupWebSocket(window.username, window.token);

// --- CUSTOM SVG BUTTONS --- //
// elaborate button code
elaborateButton.setAttribute("enabled", "true");

elaborateButton.onclick = async function() {
  if (this.getAttribute("enabled") == "true") {
    window.suggestion_output_field.innerHTML = ('');
    var text = quill.getText();
    window.last_prompt = text;
    elaborate(text, window.username, window.token);
  };
};

// this reassignment looks redundant but is not. It references the ID
// more easily visible to the DOM. (it doesn't work unless we do this)
var elaborate_button = document.getElementById("elaborate_button");

elaborate_button.setAttribute("enabled", "true");

elaborate_button.onmouseover = async function() {
  if (this.getAttribute("enabled") == "true") {
    this.setAttribute("src", elaboration_hover_image);
  };
};

elaborate_button.onmousedown = async function() {
  if (this.getAttribute("enabled") == "true") {
    this.setAttribute("src", elaboration_clicked_image);
  };
};

elaborate_button.onmouseup = async function() {
  if (this.getAttribute("enabled") == "true") {
    this.setAttribute("src", elaboration_hover_image);
  };
};

elaborate_button.onmouseleave = async function() {
  if (this.getAttribute("enabled") == "true") {
    this.setAttribute("src", elaboration_button_image);
  };
};

// summarize button code
summarizeButton.setAttribute("enabled", "true");

summarizeButton.onclick = async function() {
  if (this.getAttribute("enabled") == "true") {
    window.suggestion_output_field.innerHTML = ('');
    var text = quill.getText();
    window.last_prompt = text;
    summarize(text, window.username, window.token);
  };
};

// this reassignment looks redundant but is not. It references the ID
// more easily visible to the DOM. (it doesn't work unless we do this)
var summarize_button = document.getElementById("summarize_button");

summarize_button.setAttribute("enabled", "true");

summarize_button.onmouseover = async function() {
  if (this.getAttribute("enabled") == "true") {
    this.setAttribute("src", summarization_hover_image);
  };
};

summarize_button.onmousedown = async function() {
  if (this.getAttribute("enabled") == "true") {
    this.setAttribute("src", summarization_clicked_image);
  };
};

summarize_button.onmouseup = async function() {
  if (this.getAttribute("enabled") == "true") {
    this.setAttribute("src", summarization_hover_image);
  };
};

summarize_button.onmouseleave = async function() {
  if (this.getAttribute("enabled") == "true") {
    this.setAttribute("src", summarization_button_image);
  };
};

// articulate button code
articulateButton.setAttribute("enabled", "true");

articulateButton.onclick = async function() {
  if (this.getAttribute("enabled") == "true") {
    // alert("articulate clicked");
  };
};

// this reassignment looks redundant but is not. It references the ID
// more easily visible to the DOM. (it doesn't work unless we do this)
var articulate_button = document.getElementById("articulate_button");

articulate_button.setAttribute("enabled", "true");

articulate_button.onmouseover = async function() {
  if (this.getAttribute("enabled") == "true") {
    this.setAttribute("src", connection_hover_image);
  };
};

articulate_button.onmousedown = async function() {
  if (this.getAttribute("enabled") == "true") {
    this.setAttribute("src", connection_clicked_image);
  };
};

articulate_button.onmouseup = async function() {
  if (this.getAttribute("enabled") == "true") {
    this.setAttribute("src", connection_hover_image);
  };
};

articulate_button.onmouseleave = async function() {
  if (this.getAttribute("enabled") == "true") {
    this.setAttribute("src", connection_button_image);
  };
};


var manual_scrolling = false;

suggestion_output_field.addEventListener('touchmove', async function() {
  manual_scrolling = true;
}, { passive: true });

async function autoScroll(element, cushion) {
  if (window.manual_scrolling == true) {
    if (((element.scrollHeight - element.scrollTop) - element.clientHeight) < cushion) {
      window.manual_scrolling = false;
      element.scrollTop = element.scrollHeight;
    } else {
      // pass
    }
  } else {
    element.scrollTop = element.scrollHeight;
  };
};

// write Code //

async function elaborate(text, username, token) {
  // await writeSocket.send(JSON.stringify({
  //   "username": username,
  //   "token": token,
  //   "command": "stop",
  //   "worker": window.current_worker,
  // }));
  // console.log("Requesting STOP worker: " + window.current_worker)
  window.current_worker = new_worker();
  await writeSocket.send(JSON.stringify({
    "username": username,
    "token": token,
    "command": "elaborate",
    "worker": window.current_worker,
    'info': { "prompt": text, "length": 125, "sentences": -1, "nsamples": 1, "batch_size": 1, "top_k": 40},
    "routing": { "card": 1 }
  }));
  // console.log("Requesting: " + text);
  // console.log("From Worker: " + current_worker);
};

async function summarize(text, username, token) {
  window.current_worker = new_worker();
  await writeSocket.send(JSON.stringify({
    "username": username,
    "token": token,
    "command": "summarize",
    "worker": window.current_worker,
    'info': { "prompt": text, "length": 125, "sentences": 3, "nsamples": 1, "batch_size": 1, "top_k": 2},
    "routing": { "card": 1 }
  }));
  // console.log("Requesting: " + text);
  // console.log("From Worker: " + current_worker);
};

async function rework(text, username, token) {
  window.suggestion_output_field.innerHTML = ('');
  elaborate(window.last_prompt, window.username, window.token);
};

async function continue_elaboration() {
  past_text = window.last_prompt + window.suggestion_output_field.innerHTML;
  elaborate(past_text, window.username, window.token);
};

function new_worker() {
  var today = new Date();
  var time = today.getFullYear() + '-' + today.getMonth() + '-' + today.getDate() + "_" + today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
  var salt = randomString(length = 7);
  worker_name = time + salt;
  // console.log("Creating New Worker: " + worker_name);
  return worker_name;
}

function randomString(length = 16, chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ') {
  var result = '';
  for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
  return result;
}

// Custom Context Menu //

var customContextMenu = document.getElementById('customContextMenu');
var elab_context_option = document.getElementById('elaborate_co');
var summ_context_option = document.getElementById('summarize_co');
// var cut_context_option = document.getElementById('cut_co');
// var copy_context_option = document.getElementById('copy_co');
// var paste_context_option = document.getElementById('paste_co');

elab_context_option.onclick = async function() {
  window.suggestion_output_field.innerHTML = ('');
  range = await quill.getSelection(focus = true);
  highlighted_text = await quill.getText(range.index, range.length);
  window.last_prompt = highlighted_text
  await elaborate(highlighted_text, window.username, window.token);
};

summ_context_option.onclick = async function() {
  window.suggestion_output_field.innerHTML = ('');
  range = await quill.getSelection(focus = true);
  highlighted_text = await quill.getText(range.index, range.length);
  window.last_prompt = highlighted_text
  await summarize(highlighted_text, window.username, window.token);
};

// cut_context_option.onclick = async function() {
//   range = await quill.getSelection(focus = true);
//   highlighted_text = await quill.getText(range.index, range.length);
//   await navigator.clipboard.writeText(highlighted_text);
//   await quill.deleteText(range.index, range.length);
// };

// copy_context_option.onclick = async function() {
//   range = await quill.getSelection(focus = true);
//   highlighted_text = await quill.getText(range.index, range.length);
//   await navigator.clipboard.writeText(highlighted_text);
// };

// paste_context_option.onclick = async function() {
//   clip_text = await navigator.clipboard.readText();
//   range = await quill.getSelection(focus = true);
//   if (range.length == 0) {
//     await quill.clipboard.dangerouslyPasteHTML(range.index, clip_text);
//   } else {
//     await quill.deleteText(range.index, range.length);
//     await quill.clipboard.dangerouslyPasteHTML(range.index, clip_text);
//   };
// };

editor.oncontextmenu = async function(e) {
  // e.preventDefault();
  await showCCM(e);
};

customContextMenu.onmouseleave = async function() {
  await hideCCM();
}

async function showCCM(e) {
  window.customContextMenu;
  customContextMenu.style.display = 'block';
  customContextMenu.style.left = (e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft - 235) + 'px';
  customContextMenu.style.top = (e.clientY + document.body.scrollTop + document.documentElement.scrollTop - 10) + 'px';
  return false;
};

async function hideCCM() {
  window.customContextMenu;
  customContextMenu.style.display = 'none';
  return false;
};

// window.onclick = async function() { # no longer needed probably
//   hideCCM();
// };
// End Custom Context Menu //

// Custom Key-Bindings //

// Elaboration Hotkey:
quill.keyboard.addBinding({
  key: "e",
  shortKey: true,
  handler: async function(range, context) {
    window.suggestion_output_field.innerHTML = ('');
    highlighted_text = await quill.getText(range.index, range.length);
    window.last_prompt = highlighted_text
    await elaborate(highlighted_text, window.username, window.token);
  }
});

// End Custom Key-Bindings //

// Snippet Tower //
var snippet_tower = document.getElementById("snippet_tower")
var snippet_tower_placeholder = document.getElementById("snippet_tower_placeholder")


snippet_tower_placeholder.onclick = async function() {
  window.snippet_tower;
  this.remove();
  snippet_tower.setAttribute("contenteditable", "true")
  snippet_tower.focus();
};

continue_button = document.getElementById("continue_button");
rework_button = document.getElementById("rework_button");

continue_button.onmouseover = async function() {
  this.style.color = "red";
};

continue_button.onmousedown = async function() {
  this.style.color = "red";
  this.classList.remove("shadowy-0");
};

continue_button.onmouseup = async function() {
  this.style.color = "black";
  this.classList.add("shadowy-0");
};

continue_button.onmouseleave = async function() {
  this.style.color = "black";
  this.classList.add("shadowy-0");
};

continue_button.onclick = async function() {
  continue_elaboration();
};

rework_button.onmouseover = async function() {
  this.style.color = "red";
};

rework_button.onmousedown = async function() {
  this.style.color = "red";
  this.classList.remove("shadowy-0");
};

rework_button.onmouseup = async function() {
  this.style.color = "black";
  this.classList.add("shadowy-0");
};

rework_button.onmouseleave = async function() {
  this.style.color = "black";
  this.classList.add("shadowy-0");
};

rework_button.onclick = async function() {
  rework();
};

async function disable(button) {
  button.setAttribute("enabled", false);
  button.style.color = "grey";
  button.classList.remove("shadowy-0", "shadowy-0-270")
};

// -- Load Simulation -- //
async function simulate_request() {
  window.suggestion_output_field.innerHTML = ('');
  var text = quill.getText();
  window.last_prompt = text
  elaborate(text, window.username, window.token);
}

async function natural_load_simulator(frequency) {
  while (true) {
    await new Promise(r => setTimeout(r, 100));
    rand_float = Math.random();
    if (rand_float < (frequency / 10)) {
      simulate_request();
    };
  };
};

// natural_load_simulator(0.5);

// summarize_button.setAttribute("enabled", false);
// summarize_button.setAttribute("src", summarization_disabled_image);

articulate_button.setAttribute("enabled", false);
articulate_button.setAttribute("src", connection_disabled_image);
