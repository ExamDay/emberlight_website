from django.urls import path

from . import views

app_name = "write"

urlpatterns = [path("", views.write_page, name="write_page")]
