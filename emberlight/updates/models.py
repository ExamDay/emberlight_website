from ckeditor_uploader.fields import RichTextUploadingField
from django.db import models
from django.db.models.signals import pre_save
from django.utils.text import slugify

# Create your models here.

STATUS_CHOICES = (("draft", "Draft"), ("published", "Published"))


class Update(models.Model):
    title = models.CharField(max_length=200, unique=True)
    content = RichTextUploadingField(null=True, blank=True)
    published_on = models.DateTimeField(auto_now_add=True, editable=False)
    edited_on = models.DateTimeField(auto_now=True)
    slug = models.SlugField(null=True, blank=True, unique=True)
    status = models.CharField(max_length=10, choices=STATUS_CHOICES, default="draft")

    class Meta:
        verbose_name = "Update"
        verbose_name_plural = "Updates"
        ordering = ("-published_on",)

    def __str__(self):
        return self.title


def slug_generator(sender, instance, *args, **kwargs):
    if instance.status == "published":
        instance.slug = slugify(instance.title)
    elif instance.status == "draft":
        instance.slug = None


pre_save.connect(slug_generator, sender=Update)
