from django.urls import path

from . import views

app_name = "updates"

urlpatterns = [path("", views.all_updates), path("<str:slug>/", views.update_post)]
