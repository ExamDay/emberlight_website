from django.contrib import admin

from .models import Update

# Register your models here.


def make_published(modeladmin, request, queryset):
    queryset.update(status="published")


make_published.short_description = "Mark selected stories as published"


class UpdateAdmin(admin.ModelAdmin):
    fieldsets = [
        (
            "Make a post",
            {"fields": ["title", "slug", "content", "published_on", "edited_on", "status"]},
        )
    ]
    list_display = ("title", "published_on")
    readonly_fields = ("published_on", "slug", "edited_on")
    actions = [make_published]


admin.site.register(Update, UpdateAdmin)
