from django.shortcuts import get_object_or_404
from django.shortcuts import render

from .models import Update

# Create your views here.


def all_updates(request):
    # updates = Update.objects.get(id=2)
    print("DJANGO SAYS: ", request.method, request.path, request.user)
    updates = Update.objects.all()
    # updates = updates[::-1]
    title = "Emberlight - Company Updates"
    template_name = "updates/updates.html"
    context = {"updates": updates, "title": title}
    return render(request, template_name, context)


def update_post(request, slug):
    update = get_object_or_404(Update, slug=slug)
    print(update.title)
    print(update.slug)
    print(update.content)
    print(update.status)
    # update = Update.objects.all()
    # update = update[::-1]
    title = "Emberlight - {}".format(update.title)
    template_name = "updates/post_update.html"
    context = {"update": update, "title": title}
    return render(request, template_name, context)
