from django.contrib import admin

from .models import LoggedInUser
from .models import MiscUserInfo
from .models import StripeCustomerData

# Register your models here.


class LoggedInUserAdmin(admin.ModelAdmin):
    fieldsets = [("User Info", {"fields": ["user", "session_key"]})]
    list_display = ("user", "session_key")


class StripeCustomerDataAdmin(admin.ModelAdmin):
    fieldsets = [
        (
            "User Info",
            {
                "fields": [
                    "username",
                    "email",
                    "subscription_active",
                    "subscription_start",
                    "subscription_type",
                ]
            },
        ),
        ("Customer Data", {"fields": ["customer_json_formatted", "subscription_json_formatted"]}),
    ]
    list_display = ("username", "subscription_active", "subscription_start", "subscription_type")
    readonly_fields = (
        "username",
        "email",
        "subscription_active",
        "subscription_start",
        "subscription_type",
        "customer_json",
        "subscription_json",
        "customer_json_formatted",
        "subscription_json_formatted",
    )


# username = models.CharField(max_length=200, unique=True)
# email = models.EmailField(default="None")
# pre_alpha = models.BooleanField(default=False)
# alpha = models.BooleanField(default=False)
# beta = models.BooleanField(default=False)
# indiegogo_backer = models.BooleanField(default=False)
# account_coupon = models.CharField(max_length=200)


class MiscUserInfoAdmin(admin.ModelAdmin):
    fieldsets = [
        (
            "User's Information",
            {
                "fields": [
                    "username",
                    "email",
                    "email_list",
                    "pre_alpha",
                    "alpha",
                    "beta",
                    "indiegogo_backer",
                    "account_coupon",
                    "media_account",
                    "unix_timestamp_temp_access_start",
                    "allow_temporary_access",
                ]
            },
        )
    ]
    list_display = (
        "username",
        "email",
        "email_list",
        "pre_alpha",
        "alpha",
        "beta",
        "indiegogo_backer",
        "account_coupon",
        "media_account",
        "unix_timestamp_temp_access_start",
        "allow_temporary_access",
    )


admin.site.register(LoggedInUser, LoggedInUserAdmin)
admin.site.register(StripeCustomerData, StripeCustomerDataAdmin)
admin.site.register(MiscUserInfo, MiscUserInfoAdmin)
