from django.urls import path

from . import views

app_name = "account"

urlpatterns = [
    path("", views.account, name="account"),
    path("cancel-subscription/", views.cancel_subscription, name="cancel_subscription"),
    path("activate-subscription/", views.activate_subscription, name="activate_subscription"),
    path("change-subscription/", views.change_subscription, name="change_subscription"),
    path("update-payment-method/", views.update_payment_method, name="update_payment_method"),
    path("login/", views.login_page),
    path("logout/", views.logout_request),
    path("register/", views.register_page),
]
