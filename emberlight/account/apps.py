from django.apps import AppConfig


class AccountConfig(AppConfig):
    name = "account"

    def ready(self):
        import account.signals

        print("Pre-commit gets pissy if I don't do something with {}".format(account.signals))
