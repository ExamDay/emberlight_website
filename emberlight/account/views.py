import datetime
import json
import logging
import traceback

import stripe
from decouple import config
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import authenticate
from django.contrib.auth import login
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AuthenticationForm
from django.core.mail import send_mail
from django.shortcuts import redirect
from django.shortcuts import render
from django.views.decorators.http import require_POST

from .forms import ChangeSubscriptionPlan
from .forms import NewUserForm
from .models import MiscUserInfo
from .models import StripeCustomerData

stripe.api_key = settings.STRIPE_SECRET_KEY

env = config("WEBSITE_ENV")
if env == "development":
    logging.basicConfig(
        filename="account_views.log",
        level=logging.DEBUG,
        format="[%(asctime)s|%(name)s|account/views.py|%(levelname)s] %(message)s",
    )
elif env == "production":
    logging.basicConfig(
        filename="/webapps/emberlight/logs/gunicorn_supervisor.log",
        level=logging.INFO,
        format="[%(asctime)s|%(name)s|account/views.py|%(levelname)s] %(message)s",
    )


def register_page(request):
    title = "Emberlight - Register"
    try:
        referral_code = request.GET["ref"]
    except Exception:
        referral_code = ""
    if request.method == "POST":
        form = NewUserForm(request.POST)
        context = {"title": title, "form": form, "referral_code": referral_code}
        if form.is_valid():
            if not settings.DEBUG:
                MiscUserInfo(
                    username=form.cleaned_data.get("username"), email=form.cleaned_data.get("email")
                ).save()
            user = form.save()
            username = form.cleaned_data.get("username")
            messages.success(request, "{} created".format(username))
            login(request, user)
            if referral_code == "":
                return redirect("/pricing")
            else:
                return redirect("/pricing/?ref={}".format(referral_code))
        else:
            if referral_code == "":
                return render(request, "account/register.html", context)
            else:
                return render(
                    request, "account/register.html/?ref={}".format(referral_code), context
                )
    form = NewUserForm()
    context = {"title": title, "form": form, "referral_code": referral_code}
    return render(request, "account/register.html", context)


def login_page(request):
    title = "Emberlight - Login"
    try:
        next_page = request.GET["next"]
    except Exception:
        next_page = ""
    try:
        referral_code = request.GET["ref"]
    except Exception:
        referral_code = ""
    if request.method == "POST":
        form = AuthenticationForm(request, data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get("username")
            password = form.cleaned_data.get("password")
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                messages.success(request, "Logged in as " + username)
                if next_page == "":
                    return redirect("/")
                else:
                    return redirect(next_page)
            else:
                context = {"title": title, "form": form, "referral_code": referral_code}
                if referral_code == "":
                    return redirect("account/login", context)
                else:
                    return redirect("account/login/?ref={}".format(referral_code), context)
        else:
            context = {"title": title, "form": form, "referral_code": referral_code}
            if referral_code == "":
                return redirect("account/login/", context)
            else:
                return redirect("account/login/?ref={}".format(referral_code), context)
    form = AuthenticationForm()
    context = {"title": title, "form": form, "referral_code": referral_code}
    return render(request, "account/login.html", context)


def logout_request(request):
    logout(request)
    messages.info(request, "Logged out")
    return redirect("/")


@login_required
def account(request):
    title = "Emberlight - Account"
    try:
        logged_in_user = StripeCustomerData.objects.all().get(username=request.user)
        # print("GETTING USER'S SUBSCRIPTION STATUS")
        cancel_at_period_end = str(logged_in_user.subscription_json["cancel_at_period_end"])
    except Exception:
        # print(e)
        cancel_at_period_end = "unavailable"
    try:
        logged_in_user = (
            StripeCustomerData.objects.all().get(username=request.user).subscription_json
        )
        if (logged_in_user["status"] == "active") or (logged_in_user["status"] == "trialing"):
            subscription_type = logged_in_user["plan"]["nickname"]
        else:
            subscription_type = "unavailable"
    except Exception as e:
        print(e)
        subscription_type = "unavailable"
    context = {
        "title": title,
        "cancel_at_period_end": cancel_at_period_end,
        "subscription_type": subscription_type,
        "STRIPE_PUBLISHABLE_KEY": settings.STRIPE_PUBLISHABLE_KEY,
    }
    return render(request, "account/account.html", context)


@login_required
@require_POST
def cancel_subscription(request):
    if (
        str(request.user)
        in tuple((x[0] for x in tuple(StripeCustomerData.objects.all().values_list("username"))))
        and StripeCustomerData.objects.all()
        .get(username=request.user)
        .subscription_json["cancel_at_period_end"]
        is False
    ):
        # set subscription to cancel at period end
        subscription = StripeCustomerData.objects.all().get(username=request.user)
        stripe.Subscription.modify(subscription.subscription_json["id"], cancel_at_period_end=True)
        subscription.subscription_json["cancel_at_period_end"] = True
        subscription.save()
        # craft and send confirmation email
        try:
            customer_email = subscription.customer_json["email"]
            subscription_type = subscription.subscription_json["plan"]["nickname"]
            name = "Subscription Cancellation Confirmation"
            body = (
                "Your Emberlight "
                + subscription_type
                + " subscription has been successfully cancelled and will not be auto-renewed.\n\n"
                + "Emberlight services will remain available to you until the end of the last"
                + " period for which you were billed."
            )
            message = "{}\n{}\n\n{}".format(settings.EMAIL_HOST_USER, datetime.datetime.now(), body)
            send_mail(name, message, settings.EMAIL_HOST_USER, [customer_email])
        except Exception:
            name = "Confirmation Email Exception"
            message = "{}\n{}\n\n{}".format(
                settings.EMAIL_HOST_USER, datetime.datetime.now(), traceback.format_exc()
            )
            logging.error("\n" + name + "\n" + message + "\n")
            send_mail(name, message, settings.EMAIL_HOST_USER, ["engineering@torchlightintel.com"])
            traceback.print_exc()
    return redirect("/account")


@login_required
@require_POST
def activate_subscription(request):
    if (
        str(request.user)
        in tuple((x[0] for x in tuple(StripeCustomerData.objects.all().values_list("username"))))
        and StripeCustomerData.objects.all()
        .get(username=request.user)
        .subscription_json["cancel_at_period_end"]
        is True
    ):
        subscription = StripeCustomerData.objects.all().get(username=request.user)
        stripe.Subscription.modify(subscription.subscription_json["id"], cancel_at_period_end=False)
        subscription.subscription_json["cancel_at_period_end"] = False
        subscription.save()
        # craft and send confirmation email
        try:
            customer_email = subscription.customer_json["email"]
            subscription_type = subscription.subscription_json["plan"]["nickname"]
            name = "Subscription Activation Confirmation"
            body = (
                "Your Emberlight "
                + subscription_type
                + " subscription has been re-activated and will auto-renew like normal.\n\n"
                + "Your access to Emberlight services, if cancelled, has been restored."
            )
            message = "{}\n{}\n\n{}".format(settings.EMAIL_HOST_USER, datetime.datetime.now(), body)
            send_mail(name, message, settings.EMAIL_HOST_USER, [customer_email])
        except Exception:
            name = "Confirmation Email Exception"
            message = "{}\n{}\n\n{}".format(
                settings.EMAIL_HOST_USER, datetime.datetime.now(), traceback.format_exc()
            )
            logging.error("\n" + name + "\n" + message + "\n")
            send_mail(name, message, settings.EMAIL_HOST_USER, ["engineering@torchlightintel.com"])
            traceback.print_exc()
    return redirect("/account")


# CHECK FOR TEST VS LIVE MODE
@login_required
@require_POST
def change_subscription(request):
    title = "Emberlight - Account"
    if request.method == "POST":
        # TODO: Make sure to handle for users who try to switch to an invalid plan
        form = ChangeSubscriptionPlan(request.POST)
        print(form)
        if form.is_valid():
            selected_plan = form.cleaned_data.get("plan")
            print(selected_plan)
            try:
                plan_id = settings.PLANS[selected_plan]["stripe_id"]
            except KeyError:
                name = "THREAT: Invalid Payment Plan"
                message = "{}\n{}\n\n{}\n\n{}\n{}\n\nTraceback: {}".format(
                    settings.EMAIL_HOST_USER,
                    datetime.datetime.now(),
                    "Someone tried to change to a subscription plan we don't support.",
                    request.user,
                    request.user.email,
                    traceback.format_exc(),
                )
                send_mail(
                    name, message, settings.EMAIL_HOST_USER, ["engineering@torchlightintel.com"]
                )
                messages.error(
                    request, "Invalid subscription plan. This incident has been reported."
                )
                title = "Emberlight - Pricing"
                context = {"title": title}
                return redirect("/account", context)

    if (
        str(request.user)
        in tuple((x[0] for x in tuple(StripeCustomerData.objects.all().values_list("username"))))
        and StripeCustomerData.objects.all().get(username=request.user).subscription_active is True
    ):
        subscription = StripeCustomerData.objects.all().get(username=request.user)
        subscription_retrieval = stripe.Subscription.retrieve(subscription.subscription_json["id"])
        changed_subscription_json = stripe.Subscription.modify(
            subscription.subscription_json["id"],
            cancel_at_period_end=False,
            items=[{"id": subscription_retrieval["items"]["data"][0].id, "plan": plan_id}],
        )
        subscription.subscription_json = changed_subscription_json
        subscription.subscription_type = request.POST.get("plan")
        # subscription.subscription_active = True
        subscription.save()
    return redirect("/account", {"title": title})


# CHECK FOR TEST VS LIVE MODE
@login_required
@require_POST
def update_payment_method(request):
    title = "Emberlight - Account"
    print("Payment change request received")
    print(request.POST["stripeToken"])
    print(type(request.POST["stripeToken"]))
    print(dir(request.POST["stripeToken"]))

    stripe_customer = StripeCustomerData.objects.all().get(username=request.user)
    logged_in_user_customer_ID = stripe_customer.customer_json["id"]
    old_card_source = stripe_customer.customer_json["default_source"]
    new_card_source = stripe.Customer.create_source(
        logged_in_user_customer_ID, source=request.POST["stripeToken"]
    )
    try:
        stripe.Customer.delete_source(logged_in_user_customer_ID, old_card_source)
    except stripe.error.InvalidRequestError:
        pass
    modified_customer = stripe.Customer.modify(
        logged_in_user_customer_ID, default_source=new_card_source
    )
    stripe_customer.customer_json = json.loads(str(modified_customer))
    stripe_customer.save()

    return redirect("/account", {"title": title})
