import main.routing
import write.routing
from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter
from channels.routing import URLRouter
from channels.security.websocket import OriginValidator

websocket_urlpatterns_list = (
    main.routing.websocket_urlpatterns + write.routing.websocket_urlpatterns
)

application = ProtocolTypeRouter(
    {
        # (http->django views is added by default)
        "websocket": OriginValidator(
            AuthMiddlewareStack(URLRouter(websocket_urlpatterns_list)), ["*"]
        )
    }
)
