# SAI

![Homepage](repo_images/homepage.png)

SAI is a webapp for a text generation AI for the purpose of writing faster, better, and more creatively.

This webapp uses the GPT-2 language algorithm developed by OpenAI to create convincing continuations of any written work.

# Setup
1. Clone repo

`git clone https://github.com/Mockapapella/emberlight.git`

2. Virtual Environment

Create a virtual environment in the root of the directory and enter it with `python3 -m virtualenv venv-django && source venv-django/bin/activate`

3. Switch to `develop` branch

`git checkout develop`

4. Install Requirements

`pip install -r requirements.txt`

5. Download the AI model

`cd emberlight/ai/ && mkdir bin && cd bin && curl --output gpt2-pytorch_model.bin https://s3.amazonaws.com/models.huggingface.co/bert/gpt2-pytorch_model.bin`

6. Environment Variables

One of the current members of the team has access to the `.env` file required to run the website. This file contains details like the secret key, debug mode, database name, database user, password, and host IP. This file goes in the root of the git repo.


# Development
Make sure you're developing on a debian based linux system (Preferably Ubuntu 18.04). If you aren't, then forego Microshaft and become one with the flightless fowl.

Navigate to the root of the git repo. Issue the command `python3 manage.py runserver` and go to `127.0.0.1:8000` in a browser.

# Production
TBD
