from pprint import pprint
from time import sleep

import redis
from decouple import config


# -----------------------------------------------------------

# -- database -- #
rdb = redis.Redis(
    host="localhost", port=6379, db=0, password=config("REDIS_PASSWORD"), decode_responses=True
)

# -- monitor loop -- #
if __name__ == "__main__":
    while True:
        sleep(1)
        pipe = rdb.pipeline()
        pipe.zrange("ai_channels", 0, -1, withscores=True)
        pipe.zrange("ai_lrt", 0, -1, withscores=True)
        pipe.zrange("active_instances", 0, -1, withscores=True)
        pipe.hgetall("instance_channel_map")
        pipe_return = pipe.execute()
        ai_channels = pipe_return[0]
        ai_lrt = pipe_return[1]
        active_ais = pipe_return[2]
        instance_channel_map = pipe_return[3]
        print("\n\n#############################################################")
        print("\n\nai_channels:")
        pprint(ai_channels)
        print("\n\nai_lrt:")
        pprint(ai_lrt)
        print("\n\nactive_instances:")
        pprint(active_ais)
        print("\n\ninstance_channel_map:")
        pprint(instance_channel_map)
        print("\n\n", len(ai_channels), len(ai_lrt), len(active_ais), len(instance_channel_map))
