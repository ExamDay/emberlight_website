import asyncio
import json
import logging
import os
import sys
from pprint import pprint
from random import choice
from time import time
from warnings import filterwarnings

import boto3
import matplotlib.pyplot as plt
import numpy as np
import redis
from decouple import config
from scipy.optimize import minimize
from scipy.special import factorial

# -----------------------------------------------------------
# -- settings -- #
garrison = int(config("GARRISON"))  # number of instances in garrison

filterwarnings("error")  # catch warnings in try/except blocks
# -- control loop settings -- #
env = config("BLUE_MESA_ENV")  # environment label
if env == "development":
    development = True  # development mode
elif (env == "production") or (env == "test"):
    development = False  # production mode
update_period = 1  # period of the load history update cycle
control_period = 5  # period of the system control cycle
desired_load = 1  # desired load as a multiple of ideal (1=perfect, 0.8=underload, 1.2=overload)
avg_window = 5  # window over which to compute rolling average of load data
smoothing_window = 5  # window over which to compute rolling average of derivatives of load data.
max_billing_waste_fraction = 0.1  # maximum fraction of a billing cycle to waste on an EC2 instance.
interval = 600  # Seconds of history to consider when making control decisions.
record_gap_tolerance = 5  # largest acceptable gap in record of load history.
# ^ (decisions are forstalled until no gaps greater than or equal to this length exist.)
ideal_worker_parellelism = 2  # ideal number of jobs/workers to be running on an EC2 instance.


# -- logging -- #
logging.basicConfig(
    filename="blue_mesa.log", filemode="w", format="%(name)s - %(levelname)s - %(message)s"
)

# -- database -- #
rdb = redis.Redis(
    host="localhost", port=6379, db=0, password=config("REDIS_PASSWORD"), decode_responses=True
)


# -- AWS -- #
ec2 = boto3.Session(profile_name="emberlight-compute-manager").resource("ec2")
known_instances = eval(config("EC2_INSTANCES"))
client = boto3.Session(profile_name="emberlight-compute-manager").client("ec2")
instance_boot_time = int(config("INSTANCE_BOOT_TIME"))
ai_time_to_live = 30
interrupted_ais = {}

# -- Failsafe -- #
reboots_last_hour = 0

# -- Tools: -- #
# def utc_now():
#     return datetime.datetime.now(datetime.timezone.utc).timestamp()

# np.random.seed(100)

# -- Clean up for testing -- #
if env == "development":
    # kill all ai
    os.system("kill -9 $(pgrep -f compute_sim)")
    # wipe database of all relevant keys
    pipe = rdb.pipeline()
    pipe.delete("ai_channels")
    pipe.delete("ai_lrt")
    pipe.delete("active_instances")
    pipe.delete("inbound_instances")
    pipe.delete("instance_channel_map")
    pipe.delete("load_history")
    pipe.delete("request_history")
    pipe.delete("connection_history")
    pipe.execute()

# -- plotting variables -- #
start_time = time()
request_averages = []
work_averages = []
request_lambdas = []
load_lambdas = []


# -- tools -- #
def mark_and_json(
    data, length=8, chars="0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
):
    random_string = ""
    for i in range(length):
        random_string += choice(tuple(chars))
    unique_tuple = (data, random_string)
    return json.dumps(unique_tuple, ensure_ascii=False).encode("utf8")


# -- fancy mathematics For advanced behaviour: -- #
def make_histogram_from_event_times(event_history):
    """ Takes a list (any iterable) of timestamps and makes a histogram
    of event frequencies in Hertz -- presuming that each timestamp represents
    the time of a particular event."""
    if event_history == []:
        return [0]
    start_edge = event_history[0]
    a = 0
    c = 0
    hist = [0]
    for event in event_history:
        if event - start_edge > 1:
            for n in range(int((event - start_edge) // 1) - 1):
                hist.append(0)
                a += 1
            start_edge = event
            hist.append(1)
            c += 1
        hist[c] += 1
    for n in range(int((time() - event_history[-1]) // 1)):
        hist.append(0)
    return hist


def poisson(k, lamb):
    """poisson pdf, parameter lamb is the fit parameter"""
    try:
        return (lamb ** k / factorial(k)) * np.exp(-lamb)
    except RuntimeWarning:
        print("\n\n", RuntimeWarning)
        print("\n\nPoisson:\nK=", k, "\n\nlamb=", lamb)
        return (lamb ** k / factorial(k)) * np.exp(-lamb)


def neg_log_likelihood(params, data):
    """ performs the Negative-Log-Likelohood Function on it's arguments. """
    nll = -np.sum(np.log(poisson(data, params[0])))
    return nll


def fit_histogram_and_plot(
    load_hist,
    request_hist,
    bins=50,
    fit_range=25,
    title="Histogram with Poissonian Fit",
    ofilename="noname",
):
    # fetch and process data
    np.seterr(divide="ignore")  # make sure numpy RuntimeWarnings are actually raised as such.
    load_poissonian_fit = minimize(
        neg_log_likelihood,  # function to minimize
        x0=np.ones(1),  # start value
        args=(load_hist),  # additional arguments for function
        method="SLSQP",  # minimization method, see docs
        bounds=[(0.00001, np.inf)],
    )
    request_poissonian_fit = minimize(
        neg_log_likelihood,  # function to minimize
        x0=np.ones(1),  # start value
        args=(request_hist),  # additional arguments for function
        method="SLSQP",  # minimization method, see docs
        bounds=[(0.00001, np.inf)],
    )
    np.seterr(divide=None)
    # record parameters
    global request_lambdas
    global load_lambdas
    request_lambda = request_poissonian_fit.x[0]
    load_lambda = load_poissonian_fit.x[0]
    request_lambdas.append(request_lambda)
    load_lambdas.append(load_lambda)

    x_1 = np.linspace(0, fit_range, 1000)
    y_1 = load_poissonian_fit.x
    x_2 = np.linspace(0, fit_range, 1000)
    y_2 = request_poissonian_fit.x
    # make plot of figure with multiple subplots
    fig = plt.figure(figsize=[3, 5])
    gs = fig.add_gridspec(2, 1)
    ax1 = fig.add_subplot(gs[1, 0])
    ax2 = fig.add_subplot(gs[0, 0])
    fig.suptitle("TEST")
    # subplot 1
    ax1.set_xticks(ticks=range(5, 76, 5))
    ax1.set_yticks(ticks=range(11))
    ax1.hist(load_hist, bins, color="#0504aa")
    ax1.plot(x_1, 17.5 * poisson(x_1, y_1), "r-", lw=2)
    ax1.set_xlabel("Value")
    ax1.set_ylabel("Frequency")
    tick_label_size = 5
    ax1.tick_params(labelsize=tick_label_size)
    ax1.autoscale(enable=False)
    ax1.grid()
    # subplot 2
    ax2.set_xticks(ticks=range(5, 76, 5))
    ax2.set_yticks(ticks=range(11))
    ax2.hist(request_hist, bins, color="#0504aa")
    ax2.plot(x_2, 17.5 * poisson(x_2, y_2), "r-", lw=2)
    ax2.set_xlabel("Value")
    ax2.set_ylabel("Frequency")
    ax2.tick_params(labelsize=tick_label_size)
    ax2.autoscale(enable=False)
    ax2.grid()
    # save / display
    # plt.show()
    plt.savefig("histograms/" + ofilename + str(time()) + ".png")
    plt.close()


def plot_curves(curves, title="Curves", ofilename="curves", graph_interval=900):
    """Takes a list/tuple of lists/tuples of equal lengths (important). Each item
    in each series must be a numerical value. These correspond to y-values
    of curves to be plotted together on the same axes. Saves plot to a file in "curves" folder"""
    global start_time
    colors = ("r-", "g-", "b-", "p-")
    # benis inspection day
    bar = 0
    for curve in curves:
        length = len(curve)
        if length > bar:
            bar = length
    x_series = range(bar)
    plt.figure(figsize=[12, 2])
    i = 0
    for curve in curves:
        plt.plot(x_series, curve, colors[i], lw=2)
        plt.xticks(ticks=range(15, 901, 15), fontsize=5)
        plt.yticks(ticks=range(5, 56, 5), fontsize=5)
        # plt.xlabel("time", fontsize=9)
        # plt.ylabel("value", fontsize=9)
        plt.grid()
        # plt.title(title, fontsize=12)
        i += 1
    # plt.show()
    # -- start new graph if necessary -- #
    if (time() - start_time) > graph_interval:
        start_time = time()
    plt.savefig("curves/" + ofilename + str(start_time) + ".png")
    plt.close()


# -- Persistent Routines -- #
def update_work_history(database=rdb):
    # this adds a tuple like (25, 285.96, 1564852340.1546857) or (ai_count, encoded_load, timestamp)
    # to the list "load_history" in database.
    detailed_load_state = database.zrange("ai_channels", "0", "-1", withscores=True)
    current_ai_pop = len(detailed_load_state)
    current_ai_load = sum(x[1] for x in detailed_load_state)
    distilled_load_state = (current_ai_pop, current_ai_load, time())
    database.rpush(
        "load_history", json.dumps(distilled_load_state, ensure_ascii=False).encode("utf8")
    )
    sys.stdout.write("\r" + str(distilled_load_state) + "                         ")
    sys.stdout.flush()
    # print("\n\n")
    # pprint(detailed_load_state)


# -- Control Loop Functions -- #
def get_work_history(database=rdb, interval=600, record_gap_tolerance=5):
    """ gets last [interval] items from load record and decodes them, returning
    a list of tuples like (number_of_workers, decoded_load, timestamp) that is [interval]
    tuples long. "decoded_load" is analogous to the number of workers, system-wide, with a small
    fudge between 0 and +10% to represent their cumulative individual loads."""
    # getting encoded load information:
    recent_load_record = [json.loads(x) for x in database.lrange("load_history", -interval, -1)]
    # -- checking for validity -- #
    now = time()
    try:
        if (
            (now - (interval + record_gap_tolerance))
            < recent_load_record[0][2]
            < (now - (interval - record_gap_tolerance))
        ) and ((now - record_gap_tolerance) < recent_load_record[-1][2] < (now)):
            # decoding load information:
            recent_load_info = [(x[0], x[1] / 10, x[2]) for x in recent_load_record]
            # return recent load info if valid, otherwise return nothing.
            return (
                recent_load_info  # list of tuples like (number_of_workers, decoded_load, timestamp)
            )
        else:
            # print("Recieved incomplete information about load_history.")
            recent_load_info = [(x[0], x[1] / 10, x[2]) for x in recent_load_record]
            return (
                recent_load_info  # list of tuples like (number_of_workers, decoded_load, timestamp)
            )
    except IndexError:
        pass


def get_request_history(database=rdb, interval=600):
    """ gets last [interval] items from request record and decodes them, returning
    a list of tuples like ((user, hash), timestamp) that is [interval] tuples long."""
    # getting encoded load information:
    recent_request_record = database.zrange("request_history", -interval, -1, withscores=True)
    if recent_request_record == []:
        recent_request_history = []
    else:
        recent_request_history = [(json.loads(x), y) for x, y in recent_request_record]
    return recent_request_history  # list of tuples like ((user, hash), timestamp)


def rally_simulations(number: int, free_instances: list, database=rdb):
    completed_corrections = 0
    pipe = database.pipeline()
    for _ in range(number):
        if len(free_instances) != 0:
            instance_id = free_instances[0]
            os.system("python3 " + config("SIMULATOR_LOCATION") + instance_id + " &")
            pipe.zadd("inbound_instances", {instance_id: time()})
            print("started: " + instance_id)
            completed_corrections += 1
            del free_instances[0]
        else:
            print("No free instances available.")
            break
    pipe.execute()
    print("made correction: " + str(completed_corrections))


def rally_ais(number: int, free_instances: list):
    completed_corrections = 0
    for _ in range(number):
        if len(free_instances) != 0:
            instance_id = free_instances[0]
            correction_made = summon_ec2(instance_id)
            # -- make sure a correction is made by selecting new instances if the last instance
            # was in an unwakable state, until rally_ai returns True or we run out of
            # unused instances  -- #
            while not correction_made:
                if len(free_instances) != 0:
                    free_instances = list(set(free_instances) - set(instance_id))
                    instance_id = free_instances[0]
                    correction_made = summon_ec2(instance_id)
                else:
                    print("No wakeable instances available.")
                    break
            if correction_made:
                completed_corrections += 1
                print("started: " + instance_id)
            del free_instances[0]
        else:
            print("No free instances available.")
            break
    print("made correction: " + str(completed_corrections))


def kill_simulation(doomed_instance_id: str):
    os.system("kill -9 $(pgrep -f " + doomed_instance_id + ")")
    print("killed: " + doomed_instance_id)


def kill_ai(doomed_instance_id: str):
    instance = ec2.Instance(doomed_instance_id)
    instance.stop()
    print("killed: " + doomed_instance_id)


def summon_ec2(instance_id, database=rdb):
    global instance_boot_time
    global reboots_last_hour
    # -- AI Startup -- #
    instance = ec2.Instance(instance_id)
    state = instance.state["Name"]
    try:  # "come over."
        if state in ("stopped", "terminated"):
            instance.start()
            database.zadd("inbound_instances", {instance_id: time()})
            return True

        elif state == "pending":
            return True

        elif state in ("stopping", "shutting-down"):
            return False

        elif state == "running":  # "no."
            # this means there is an instance running that has either not reported in
            # or has somehow been removed from our records. So we should send a reconnect/reset
            # command to the program here (if possible) instead of hard restarting like we do now.
            launch_time = (
                client.describe_instances(InstanceIds=[instance_id])["Reservations"][0][
                    "Instances"
                ][0]["LaunchTime"]
            ).timestamp()
            time_now = time()
            if ((time_now - launch_time) >= instance_boot_time) and (reboots_last_hour < 3):
                instance.reboot()  # "my parent's aren't home."
                reboots_last_hour += 1
                database.zadd(
                    "inbound_instances", {instance_id: time() + instance_boot_time}
                )  # give the instance a little extra time because reboots are slower than startups
                return True
            else:
                return True
        else:
            print("Wut the fuk?")
            return False
    except Exception as e:
        print("\nException in rally_ai:\n", e, "\n")
        return False


def watchdog(database=rdb):
    global instance_boot_time
    global ai_time_to_live
    # -- fetch needed data and trim as needed to preserve memory -- #
    pipe = database.pipeline()
    pipe.zrange("ai_channels", 0, -1)
    pipe.zrange("ai_lrt", 0, -1, withscores=True)
    pipe.hgetall("instance_channel_map")  # returns dictionary
    pipe.zrange("inbound_instances", 0, -1, withscores=True)
    pipe.zrange("active_instances", 0, -1, withscores=True)
    # trim work histories to contain information only from the last hour.
    pipe.ltrim("load_history", -3600, -1)  # this list grows by 1 item each second (if blue
    # mesa is running ), so asking for the last 3600 items gets us the last hour.
    pipe.zremrangebyscore("request_history", 0, time() - 3600)  # this list grows with every request
    # and does not have a constant time between each entry, so we are being careful to trim it
    # by time interval and not rank. This also gets us the last hour of data.
    pipe_return = pipe.execute()
    # set variables as appropriate
    ai_channels = pipe_return[0]
    ai_lrt = pipe_return[1]
    instance_channel_map = pipe_return[2]
    inbound_instance_data = pipe_return[3]
    # handle for non-extant variables (resulting in nonetypes) and convert to dicts as needed.
    if ai_lrt is None:
        ai_lrt = {}
    else:
        ai_lrt = dict(ai_lrt)
    if instance_channel_map is None:
        instance_channel_map = {}
    else:
        instance_channel_map = dict(instance_channel_map)
    if inbound_instance_data is None:
        inbound_instance_data = {}
    else:
        inbound_instance_data = dict(inbound_instance_data)
    # -- find dead ai channels and remove them -- #
    pipe = database.pipeline()
    # get list of dead channels
    dead_channels = []
    dead_instances = []
    reported_ais = tuple(ai_lrt.keys())
    for instance_id in reported_ais:
        # check for instances that have gone too long without reporting in and
        # slate them for purging.
        if (time() - ai_lrt[instance_id]) > ai_time_to_live:
            dead_instances.append(instance_id)
            try:
                dead_channel = instance_channel_map[instance_id]
                dead_channels.append(dead_channel)
            except KeyError:
                pass
    # reverse instance channel map
    channel_instance_map = dict([(v, k) for k, v in instance_channel_map.items()])
    # -- purge the dead -- #
    pipe = database.pipeline()
    for channel_name in dead_channels:
        pipe.zrem("ai_channels", channel_name)
        try:
            dead_instance_id = channel_instance_map[channel_name]
            pipe.zrem("ai_lrt", dead_instance_id)
            pipe.hdel("instance_channel_map", dead_instance_id)
            pipe.zrem("active_instances", dead_instance_id)
        except KeyError as e:
            print("Watchdog dead_channel KeyError: ", e)
            pass
        print("Watchdog killed dead channel: ", channel_name)
    for instance_id in dead_instances:
        pipe.zrem("ai_lrt", instance_id)
        pipe.zrem("active_instances", instance_id)
        pipe.hdel("instance_channel_map", instance_id)
    for channel_name in ai_channels:
        if channel_name not in channel_instance_map:
            pipe.zrem("ai_channels", channel_name)
    # -- cull stalling instances -- #
    for item in inbound_instance_data.keys():
        if (time() - inbound_instance_data[item]) > instance_boot_time:
            pipe.zrem("inbound_instances", item)
    pipe.execute()


def experiment():
    # construct histogram of load history and fit with poissonian.
    work_history = get_work_history(interval=900)
    total_inst_work_history = [x[1] for x in work_history]
    worker_totals = [x[0] for x in work_history]
    # START TESTING SETTINGS:
    # N = 1000
    # hist = np.random.poisson(lam=10, size=N)
    # END TESTING SETTINGS
    # Use elements above to create histogram and fit with plot:
    request_history = get_request_history()
    if request_history != []:
        # truncate series of request times to only those that are within desired interval of now
        request_times = [x[1] for x in request_history]
        desired_interval = 900
        if (time() - request_times[-1]) < desired_interval:
            i = 0
            for timestamp in request_times:
                if (time() - timestamp) < desired_interval:
                    request_times = request_times[i::]
                    break
                i += 1
    else:
        request_times = []
    request_hist = make_histogram_from_event_times(request_times)
    # fit_histogram_and_plot(
    # load_hist=total_inst_work_history[-30::], request_hist=request_hist, bins=30, fit_range=75
    # )
    global work_averages
    # -- adding to new entries to data series -- #
    # work average
    avg_window = 15
    length = len(total_inst_work_history)
    if length < avg_window:
        lengthened_list = [0 for x in range(avg_window - length)] + total_inst_work_history
        work_averages.append(sum(lengthened_list) / avg_window)
    else:
        work_averages.append(sum(total_inst_work_history[-avg_window::]) / avg_window)
    # request average
    global request_averages
    length = len(request_hist)
    if length < avg_window:
        lengthened_list = [0 for x in range(avg_window - length)] + request_hist
        request_averages.append(sum(lengthened_list) / avg_window)
    else:
        request_averages.append(sum(request_hist[-avg_window::]) / avg_window)
    # stretching data series to match sample frequency of worker totals (5x as frequent sampling)
    stretch_factor = 5
    stretched_request_averages = []
    for item in request_averages:
        stretched_request_averages += [item for x in range(stretch_factor)]
    stretched_work_averages = []
    for item in work_averages:
        stretched_work_averages += [item for x in range(stretch_factor)]
    # padding data series to match length of desired interval.
    # (adding zeros to beginning as needed to properly graph them on the same axes)
    desired_interval = 900
    sized_curves = []
    for series in (
        [(3 * x) for x in stretched_request_averages],
        stretched_work_averages,
        worker_totals,
    ):
        length = len(series)
        difference = length - desired_interval
        if difference > 0:
            sized_curves.append(series[-desired_interval::])
        elif difference < 0:
            sized_curves.append([0 for x in range(-difference)] + series)
        elif difference == 0:
            sized_curves.append(series)
        else:
            print("How the fuck does math even work?")
    # plotting curves
    plot_curves(sized_curves)


class system_state:
    def __init__(
        self,
        database=rdb,
        desired_load=desired_load,
        interval=interval,
        avg_window=avg_window,
        smoothing_window=smoothing_window,
        record_gap_tolerance=record_gap_tolerance,
        ideal_worker_parellelism=ideal_worker_parellelism,
    ):
        # -- fetch needed data -- #
        # fetch list of known instances.
        global known_instances
        self.known_instances = known_instances  # list of instance ids
        # fetch and format active and inbound instance data.
        pipe = database.pipeline()
        pipe.zrange("active_instances", 0, -1, withscores=True)
        pipe.zrange("inbound_instances", 0, -1, withscores=True)
        pipe_return = pipe.execute()
        self.active_instance_data = pipe_return[0]
        self.inbound_instance_data = pipe_return[1]
        # handle for non-extant variables (resulting in nonetypes) and convert to dicts as needed.
        if self.active_instance_data is None:
            self.active_instance_data = {}
        else:
            self.active_instance_data = dict(self.active_instance_data)
        if self.inbound_instance_data is None:
            self.inbound_instance_data = {}
        else:
            self.inbound_instance_data = dict(self.inbound_instance_data)
        # provide list of ids
        if len(self.active_instance_data) != 0:
            self.active_instance_ids = list(self.active_instance_data.keys())
        else:
            self.active_instance_ids = []
        if len(self.inbound_instance_data) != 0:
            print("\n\ninbound: ")
            pprint(tuple(self.inbound_instance_data.keys()))
            self.inbound_instance_ids = list(self.inbound_instance_data.keys())
        else:
            self.inbound_instance_ids = []
        # -- calculate error state -- #
        try:
            # fetch and format work history.
            work_history = get_work_history(
                interval=interval, record_gap_tolerance=record_gap_tolerance
            )
            load_per_ai = []
            for item in work_history:
                try:
                    load_per_ai.append(
                        (item[1] / (item[0] * ideal_worker_parellelism))
                    )  # as a fraction of 1 ideal work unit
                except ZeroDivisionError:
                    load_per_ai.append(0)
            N = avg_window  # window size for rolling avg.
            rolling_avg_load_history = np.convolve(load_per_ai, np.ones((N,)) / N, mode="valid")
            load_error_history = [x - desired_load for x in load_per_ai]
            rolling_avg_load_error_history = np.convolve(
                load_error_history, np.ones((N,)) / N, mode="valid"
            )
            # finding latest root if there is one, otherwise integrating from beginnning of data.
            roots = np.where(np.diff(np.sign(load_error_history)), 1, 0)
            latest_root_index = 0
            for i in range(len(roots)):
                index = -(i + 1)
                if roots[index] == 1:
                    latest_root_index = index
                    break
            self.proportional_error = rolling_avg_load_history[-1] / desired_load
            self.integral_error = sum(load_error_history[latest_root_index::])
            w = smoothing_window  # derivative window
            self.derivative_error = (
                sum(rolling_avg_load_error_history[-w::]) / w
                - sum(rolling_avg_load_error_history[-2 * w : -w]) / w
            ) / w
            self.instances = work_history[-1][0]  # instantaneous number of instances
            self.avg_load = rolling_avg_load_history[-1]  # instantaneous decoded avg load
            # signal success
            self.success = True
        except Exception as e:
            # signal failure
            print("Can't get current System State:\n", e)
            self.success = False


def calculate_correction(system_state: system_state, garrison: int = garrison):
    # set variables for convenience.
    state = system_state
    pe = state.proportional_error
    # ie = current_system_state[2]
    # de = current_system_state[3]
    # -- find recommended change in number of AI for each component of algorithm: -- #
    # (this section will be longer in the future.)
    proportional_rec = state.instances * (pe - 1)
    # -- determine proposed correction from weighted recommendations:
    # (this section will be longer in the future.) --#
    proposal = int(round(proportional_rec))
    # -- determine actual correction by checking proposition against current circumstances -- #
    # when proposition is negative, make sure to not underman garrison.
    # and if proposition would do that, correct only to garrison.
    if (proposal < 0) and ((state.instances + proposal) < garrison):
        correction = -(state.instances - garrison)
    # when proposition is positive, only make correction if proposal exceeds number of inbound
    # instances, and then correct only by the difference.
    elif (proposal < 0) and ((state.instances + proposal) >= garrison):
        correction = proposal
    elif 0 < proposal > len(state.inbound_instance_data):
        correction = proposal - len(state.inbound_instance_data)
    else:
        correction = 0
    return correction


def make_correction(
    system_state: system_state,
    correction: int = 0,
    max_billing_waste_fraction: float = max_billing_waste_fraction,
    database=rdb,
    development: bool = development,
):
    # -- set variables for convenience -- #
    state = system_state
    current_ai = state.instances
    active_instance_data = state.active_instance_data
    active_instance_ids = state.active_instance_ids
    inbound_instance_ids = state.inbound_instance_ids
    # -- make correction: --#
    billing_period = int(config("BILLING_PERIOD"))
    # if correction is positive start up new instances:
    if correction > 0:
        print("correction: " + str(correction))
        free_instances = list(
            set(known_instances) - set(inbound_instance_ids + active_instance_ids)
        )
        if development:
            rally_simulations(number=correction, free_instances=free_instances)
        elif not development:
            rally_ais(number=correction, free_instances=free_instances)
    # if correction is negative kill active instances:
    elif correction < 0:
        print("correction: " + str(correction))
        # make sure to not underman garrison.
        if (current_ai + correction) >= garrison:
            # only stop those instances that are mortal (EC2 bound)
            # and close to the end of their current billing cycle.
            mortal_instances = list(set(known_instances) & set(active_instance_ids))
            timely_instances = []
            for inst in mortal_instances:
                age = time() - active_instance_data[inst]
                if (age > (billing_period * (1 - max_billing_waste_fraction))) and (
                    (age % billing_period) < (billing_period * max_billing_waste_fraction)
                ):
                    timely_instances.append(inst)
            corrections_made = 0
            if len(timely_instances) > 0:
                # get instance to channel map
                instance_channel_map = database.hgetall("instance_channel_map")
                if instance_channel_map is None:
                    instance_channel_map = {}
                pipe = database.pipeline()
                for i in range(-correction):
                    try:  # dont just do nothing if there arent enough timely instances
                        doomed_instance = timely_instances[i]
                        pipe.zrem("ai_lrt", doomed_instance)
                        pipe.hdel("instance_channel_map", doomed_instance)
                        pipe.zrem("active_instances", doomed_instance)
                        try:
                            doomed_channel = instance_channel_map[doomed_instance]
                            pipe.zrem("ai_channels", doomed_channel)
                        except KeyError:
                            print("Instance kill KeyError")
                        if development:
                            kill_simulation(doomed_instance)
                        elif not development:
                            kill_ai(doomed_instance)
                        corrections_made -= 1
                    except IndexError:
                        pass
                pipe.execute()
            else:
                print("no timely_instances")
                pass


def control(
    garrison: int = garrison,
    database=rdb,
    development: bool = development,
    desired_load: float = desired_load,
    ideal_worker_parellelism: int = ideal_worker_parellelism,
    interval: int = interval,
    avg_window: int = avg_window,
    smoothing_window: int = smoothing_window,
    record_gap_tolerance: int = record_gap_tolerance,
    max_billing_waste_fraction: float = max_billing_waste_fraction,
):
    state = system_state()
    if state.success:
        correction = calculate_correction(garrison=garrison, system_state=state)
        make_correction(correction=correction, system_state=state)


async def control_loop(
    update_period: float = update_period,
    control_period: float = control_period,
    development: bool = development,
    desired_load: float = desired_load,
    avg_window: int = avg_window,
    smoothing_window: int = smoothing_window,
    max_billing_waste_fraction: float = max_billing_waste_fraction,
):
    global reboots_last_hour
    print("\nstarted control loop\n")
    if (env == "production") or (env == "test"):
        update_period = 1
    i = 1
    aws_reboot_reset_timer = 1
    latency = 0
    while True:
        await asyncio.sleep(update_period - latency)
        update_work_history()
        start = time()
        if i == control_period:
            watchdog()
            experiment()
            control()
            i = 0
        if aws_reboot_reset_timer >= 3600:
            reboots_last_hour = 0
            aws_reboot_reset_timer = 0
        i += 1
        aws_reboot_reset_timer += 1
        end = time()
        latency = end - start
        # print("data processing time: ", latency)
    control_loop.cancel()


async def task_generator():
    await asyncio.gather(asyncio.ensure_future(control_loop()))


# --------------------------------------------------------------------
# -- Experimental Runscripts -- #
# requests = get_work_history()[1]
# hist = make_histogram_from_event_times(requests)
# N = len(requests)
# TESTING SETTINGS:
# N = 1000
# hist = np.random.poisson(lam=10, size=N)
# Use elements above to create histogram and fit with plot:
# fit_histogram_and_plot(hist, bins=25, fit_range=25)

if __name__ == "__main__":
    env = config("BLUE_MESA_ENV")
    # configure global settings according to environment #
    if (env == "production") or (env == "test"):
        # -- DANGER: NOT TO BE OPERATED BY FUCKWITS! -- #
        development = False
        update_period = 1
        control_period = 5
        desired_load = 1.2
        avg_window = 5
        smoothing_window = 5
        max_billing_waste_fraction = 0.085
    elif env == "development":
        development = True  # production mode
        update_period = 1
        control_period = 5
        desired_load = 1
        avg_window = 5
        smoothing_window = 5
        max_billing_waste_fraction = 0.1
    # keep the control loop spinning indefinitely and fail gracefully if there's a problem #
    carousel = asyncio.get_event_loop()
    carousel.run_until_complete(task_generator())
    carousel.run_forever()
    print("Lethal fault in auto-scaling program. It ... it's shutting down!")
    carousel.close()
